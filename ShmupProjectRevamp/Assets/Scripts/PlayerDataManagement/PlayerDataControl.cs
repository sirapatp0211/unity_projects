﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class PlayerDataControl : MonoBehaviour
{
    public static PlayerDataControl pCon;

    public string selectedRaptorName;
    public Raptor selectedRaptor;

    public Color primaryColor;
    public Color secondaryColor;
    public Color tertiaryColor;

    public int highScore;
    public int score;

    public int maxHP;
    public int remainingHP;
    public int atk;
    public int def;
    public int spd;
    public int maxEnergy;
    public int remainingEnergy;

    private void Start()
    {
        if (pCon == null)
        {
            pCon = this;
            DontDestroyOnLoad(gameObject);
        }

        if (File.Exists(Application.persistentDataPath + "/playerData.sav"))
        {
            GetComponent<GameSaveLoad>().LoadGame();
        }
    }

    public void startGame()
    {
        Texture2D reticleTexture = Resources.Load("textures/cursors/target reticle cursor") as Texture2D;
        Cursor.SetCursor(reticleTexture, new Vector2(reticleTexture.width * 0.5f, reticleTexture.height * 0.5f), CursorMode.Auto);
        FindObjectOfType<EnemySpawnControl>().gameStarted = true;
        FindObjectOfType<PowerUpOrbSpawnControl>().gameStarted = true;
        FindObjectOfType<CameraMover>().gameStarted = true;
        selectedRaptor = Resources.Load<Raptor>("RaptorClasses/" + selectedRaptorName);

        score = 0;
        maxHP = selectedRaptor.maxHP;
        remainingHP = maxHP;
        atk = selectedRaptor.atk;
        def = selectedRaptor.def;
        spd = selectedRaptor.spd;
        maxEnergy = selectedRaptor.energy;
        remainingEnergy = maxEnergy;
    }
}
