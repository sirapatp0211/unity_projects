﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    void Start()
    {
        transform.SetParent(Camera.main.transform);
    }

    void Update()
    {
        if (!GetComponent<SpriteRenderer>().isVisible)
            Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemy"))
        {
            GameObject hitEnemy = collision.gameObject;
            int damageDealt = damageToDeal(PlayerDataControl.pCon.atk, hitEnemy.GetComponent<EnemyBehavior>().enemyDef);
            hitEnemy.GetComponent<EnemyBehavior>().takeDamage(damageDealt);
        }
        
        if (!collision.gameObject.tag.Equals("Player"))
            Destroy(gameObject);
    }

    private int damageToDeal(int playerAttack, int enemyDefense)
    {
        float damage = playerAttack * (1 - (enemyDefense / (enemyDefense + 50)));
        return Mathf.CeilToInt(damage);
    }
}
