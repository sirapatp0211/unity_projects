﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyBullet : MonoBehaviour
{
    public int enemyAtk;

    private void Start()
    {
        GameObject[] enemyObjects = GameObject.FindGameObjectsWithTag("Enemy");

        for (int i = 0; i < enemyObjects.Length; i++)
        {
            Physics2D.IgnoreCollision(gameObject.GetComponent<Collider2D>(), enemyObjects[i].GetComponent<Collider2D>());
        }
    }

    void Update()
    {
        if (!GetComponent<SpriteRenderer>().isVisible)
            Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            
        }

        else
        {
            if (collision.gameObject.tag.Equals("Player"))
            {
                GameObject hitEnemy = collision.gameObject;
                FindObjectOfType<PlayerDataControl>().remainingHP -= damageToDeal(FindObjectOfType<PlayerDataControl>().def);
                FindObjectOfType<GameControl>().updateHealthBar();
            }

            Destroy(gameObject);
        }
    }

    private int damageToDeal(int playerDefense)
    {
        float damage = enemyAtk * (1 - (playerDefense / (playerDefense + 50)));
        return Mathf.CeilToInt(damage);
    }
}
