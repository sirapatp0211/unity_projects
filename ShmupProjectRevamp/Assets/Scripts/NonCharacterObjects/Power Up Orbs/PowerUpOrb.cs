﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpOrb : MonoBehaviour
{
    public string powerUpType;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            if (powerUpType.Equals("health restore"))
            {
                PlayerDataControl.pCon.remainingHP += 30;
                if (PlayerDataControl.pCon.remainingHP > PlayerDataControl.pCon.maxHP)
                    PlayerDataControl.pCon.remainingHP = PlayerDataControl.pCon.maxHP;
                FindObjectOfType<GameControl>().updateHealthBar();
            }

            else if (powerUpType.Equals("energy restore"))
            {
                PlayerDataControl.pCon.remainingEnergy += 20;
                if (PlayerDataControl.pCon.remainingEnergy > PlayerDataControl.pCon.maxEnergy)
                    PlayerDataControl.pCon.remainingEnergy = PlayerDataControl.pCon.maxEnergy;
                FindObjectOfType<GameControl>().updateEnergyBar();
            }

            Destroy(gameObject);
        }

        else if (collision.gameObject.tag.Equals("Bullet"))
        {
            if (powerUpType.Equals("health restore"))
            {
                PlayerDataControl.pCon.remainingHP += 15;
                if (PlayerDataControl.pCon.remainingHP > PlayerDataControl.pCon.maxHP)
                    PlayerDataControl.pCon.remainingHP = PlayerDataControl.pCon.maxHP;
                FindObjectOfType<GameControl>().updateHealthBar();
            }

            else if (powerUpType.Equals("energy restore"))
            {
                PlayerDataControl.pCon.remainingEnergy += 10;
                if (PlayerDataControl.pCon.remainingEnergy > PlayerDataControl.pCon.maxEnergy)
                    PlayerDataControl.pCon.remainingEnergy = PlayerDataControl.pCon.maxEnergy;
                FindObjectOfType<GameControl>().updateEnergyBar();
            }

            Destroy(gameObject);
        }
    }

    private void Update()
    {
        float screenLeftEdge = Camera.main.ScreenToWorldPoint(Vector3.zero).x;

        if (gameObject.transform.position.x < screenLeftEdge - 2)
            Destroy(gameObject);
    }
}
