﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior : MonoBehaviour
{
    public string enemyName;

    public enum rarity
    {
        common,
        uncommon,
        rare,
        mythical
    }

    public rarity enemyRarity;

    public int enemyMaxHP;
    public int remainingHP;
    public int enemyAtk;
    public int enemyDef;
    public float fireRate;
    public float timeOfNextShot;

    private bool isDead;

    public int pointWorth;

    public int itemDropPercentage;

    public float timeSinceSpawn;

    private Vector3 startingPos;

    public bool facePlayer;

    public bool persistent;
    public float enemyStayPos;

    public bool sineWaveMovement;
    public float waveFrequency;
    public float waveAmplitude;

    private AudioClip enemyShotSound;
    private AudioClip enemyDeathSound;

    private void Start()
    {
        remainingHP = enemyMaxHP;
        startingPos = transform.position;
        timeOfNextShot = (1f / fireRate);
        enemyShotSound = Resources.Load<AudioClip>("Sound effects/enemy_shot");
        enemyDeathSound = Resources.Load<AudioClip>("Sound effects/enemy_death_explosion");
    }

    private void Update()
    {
        timeSinceSpawn += Time.deltaTime;

        if (facePlayer)
        {
            Vector2 enemyPosition = gameObject.transform.position;
            Vector2 playerPosition = GameObject.FindWithTag("Player").transform.position;

            Vector2 direction = playerPosition - enemyPosition;
            direction.Normalize();

            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            gameObject.transform.rotation = Quaternion.Euler(0, 0, angle);
        }

        if (persistent)
        {
            if (gameObject.transform.position.x < Camera.main.ScreenToWorldPoint(new Vector3(enemyStayPos * Screen.width, 0)).x)
            {
                Vector3 currentPos = gameObject.transform.position;
                float anchorPosX = Camera.main.ScreenToWorldPoint(new Vector3(enemyStayPos * Screen.width, 0)).x;
                gameObject.transform.position = new Vector3(anchorPosX, currentPos.y, currentPos.z);
                gameObject.transform.SetParent(Camera.main.gameObject.transform);
            }
        }

        if (sineWaveMovement)
        {
            gameObject.transform.position = new Vector3(startingPos.x, startingPos.y + (waveAmplitude * Mathf.Sin(waveFrequency * timeSinceSpawn)), startingPos.z);
        }

        if(GetComponent<SpriteRenderer>().isVisible && timeSinceSpawn >= timeOfNextShot)
        {
            Shoot();
        }

        float screenLeftEdge = Camera.main.ScreenToWorldPoint(Vector3.zero).x;

        if (gameObject.transform.position.x < screenLeftEdge - 2)
            Destroy(gameObject);
    }

    public void takeDamage(int damageToTake)
    {
        remainingHP -= damageToTake;
        if (remainingHP <= 0)
            Die();
    }

    private void Die()
    {
        PlayerDataControl.pCon.score += pointWorth;
        FindObjectOfType<GameControl>().updateScoreDisplay();
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Collider2D>().enabled = false;
        GetComponent<AudioSource>().clip = enemyDeathSound;
        GetComponent<AudioSource>().Play();
        rollForItemDrop();
        Destroy(gameObject, GetComponent<AudioSource>().clip.length);
    }

    private void rollForItemDrop()
    {
        int roll = Random.Range(0, 100);

        if (roll < itemDropPercentage)
            dropItem();
    }

    private void dropItem()
    {
        int dropNumber = Random.Range(0, 100);

        if (dropNumber >= 0 && dropNumber < 45)
        {
            //drop common item
            print("Common item dropped by " + enemyName);
        }
        else if (dropNumber >= 45 && dropNumber < 75)
        {
            //drop uncommon item
            print("Uncommon item dropped by " + enemyName);
        }
        else if (dropNumber >= 75 && dropNumber < 95)
        {
            //drop rare item
            print("Rare item dropped by " + enemyName);
        }
        else
        {
            //drop legendary item
            print("Legendary item dropped by " + enemyName);
        }
    }

    private void Shoot()
    {
        GetComponent<AudioSource>().clip = enemyShotSound;
        GetComponent<AudioSource>().Play();
        GameObject spawnedBullet = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/enemyBullet"));
        spawnedBullet.transform.position = transform.Find("shotSpawn").transform.position;
        spawnedBullet.GetComponent<enemyBullet>().enemyAtk = enemyAtk;
        spawnedBullet.GetComponent<Rigidbody2D>().velocity = new Vector3(-20, 0, 0);
        timeOfNextShot += (1f / fireRate);
    }
}
