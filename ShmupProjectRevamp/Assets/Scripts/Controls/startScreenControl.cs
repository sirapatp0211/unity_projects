﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class startScreenControl : MonoBehaviour
{
    public GameObject titleText;
    public GameObject instructionText;
    public GameObject playButton;
    public GameObject charSettingButton;
    public GameObject quitButton;
    public GameObject highScoreHeader;
    public GameObject highScoreText;

    private void Start()
    {
        Cursor.SetCursor(Resources.Load("textures/cursors/DefaultCursor") as Texture2D, Vector2.zero, CursorMode.Auto);
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0) && titleText.activeInHierarchy && instructionText.activeInHierarchy)
        {
            titleText.SetActive(false);
            instructionText.SetActive(false);

            playButton.SetActive(true);
            charSettingButton.SetActive(true);
            quitButton.SetActive(true);
            highScoreHeader.SetActive(true);
            highScoreText.SetActive(true);

            highScoreText.GetComponent<Text>().text = PlayerDataControl.pCon.highScore.ToString();
        }
    }

    public void startGame()
    {
        SceneManager.LoadScene("GameScreen");
    }

    public void toCharacterSetting()
    {
        SceneManager.LoadScene("CharacterSettings");
    }

    public void quitGame()
    {
        FindObjectOfType<GameSaveLoad>().SaveGame();
        Application.Quit();
    }
}
