﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnControl : MonoBehaviour
{
    private GameObject[] allEnemyPrefabs;
    public bool gameStarted;
    public float spawnTimer;

    private void Start()
    {
        allEnemyPrefabs = Resources.LoadAll<GameObject>("Prefabs/Enemy Prefabs");
        spawnTimer = 0f;
    }

    private void Update()
    {
        if (gameStarted)
        {
            spawnTimer -= Time.deltaTime;
            if (spawnTimer <= 0f)
            {
                spawnEnemies();
                spawnTimer = 3.0f;
            }
        }
    }

    public void spawnEnemies()
    {
        GameObject[] spawnPatterns = Resources.LoadAll<GameObject>("Prefabs/Enemy Spawn Patterns");
        int spawnPatternToUse = Random.Range(0, spawnPatterns.Length);

        Vector3 screenEdges = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        Vector3 screenCenter = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 0));

        float maxXPos = (screenEdges.x - screenCenter.x) + screenEdges.x;
        float minXPos = (screenEdges.x - screenCenter.x) * 0.6f + screenEdges.x;

        float xPos = Random.Range(minXPos, maxXPos);
        float yPos = Random.Range(-2, 2);

        GameObject spawnPattern = Instantiate<GameObject>(spawnPatterns[spawnPatternToUse]);
        spawnPattern.gameObject.transform.position = new Vector3(xPos, yPos, -2.5f);

        for (int i = 0; i < spawnPattern.transform.childCount; i++)
        {
            Transform spawnTransform = spawnPattern.transform.GetChild(i).transform;

            GameObject[] enemyPrefabs = Resources.LoadAll<GameObject>("Prefabs/Enemy Prefabs");

            GameObject[] commonEnemies = new GameObject[0];
            for (int c = 0; c < enemyPrefabs.Length; c++)
            {
                if (enemyPrefabs[c].GetComponent<EnemyBehavior>().enemyRarity == EnemyBehavior.rarity.common)
                {
                    commonEnemies = new GameObject[commonEnemies.Length + 1];
                    commonEnemies[commonEnemies.Length - 1] = enemyPrefabs[c];
                }
            }

            GameObject[] uncommonEnemies = new GameObject[0];
            for (int u = 0; u < enemyPrefabs.Length; u++)
            {
                if (enemyPrefabs[u].GetComponent<EnemyBehavior>().enemyRarity == EnemyBehavior.rarity.uncommon)
                {
                    uncommonEnemies = new GameObject[uncommonEnemies.Length + 1];
                    uncommonEnemies[uncommonEnemies.Length - 1] = enemyPrefabs[u];
                }
            }

            GameObject[] rareEnemies = new GameObject[0];
            for (int r = 0; r < enemyPrefabs.Length; r++)
            {
                if (enemyPrefabs[r].GetComponent<EnemyBehavior>().enemyRarity == EnemyBehavior.rarity.rare)
                {
                    rareEnemies = new GameObject[rareEnemies.Length + 1];
                    rareEnemies[rareEnemies.Length - 1] = enemyPrefabs[r];
                }
            }

            GameObject[] mythicalEnemies = new GameObject[0];
            for (int m = 0; m < enemyPrefabs.Length; m++)
            {
                if (enemyPrefabs[m].GetComponent<EnemyBehavior>().enemyRarity == EnemyBehavior.rarity.mythical)
                {
                    mythicalEnemies = new GameObject[mythicalEnemies.Length + 1];
                    mythicalEnemies[mythicalEnemies.Length - 1] = enemyPrefabs[m];
                }
            }

            float spawnedEnemyRarity = Random.Range(0f, 1f);

            GameObject spawnedEnemy;

            //adjust the conditions below once enemies of new rarity are added.
            if (spawnedEnemyRarity < 0.75f)
            {
                spawnedEnemy = spawnedEnemyFromRarityTier(commonEnemies);
                spawnedEnemy.transform.position = spawnTransform.position;
            }

            else
            {
                spawnedEnemy = spawnedEnemyFromRarityTier(uncommonEnemies);
                spawnedEnemy.transform.position = spawnTransform.position;
            }
        }
        Destroy(spawnPattern);
    }

    private GameObject spawnedEnemyFromRarityTier(GameObject[] rarityTierList)
    {
        int enemyToSpawn = Random.Range(0, rarityTierList.Length);
        print(enemyToSpawn);
        GameObject spawnedEnemy = Instantiate<GameObject>(rarityTierList[enemyToSpawn]);
        return spawnedEnemy;
    }
}
