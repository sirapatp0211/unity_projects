﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour
{
    public GameObject charSelectUI;
    public GameObject gameUI;
    public GameObject endGameUI;
    public GameObject newHighScoreAlert;

    public GameObject scoreText;
    public GameObject endGameScoreText;

    public GameObject healthBar;
    public GameObject energyBar;

    private AudioClip deathSoundEffect;

    private float timePassed;

    private void Start()
    {
        timePassed = 0;

        if (PlayerDataControl.pCon.selectedRaptorName != "")
        {
            charSelectUI.SetActive(false);
            gameUI.SetActive(true);
            PlayerDataControl.pCon.startGame();

            Instantiate<GameObject>(Resources.Load("Prefabs/" + PlayerDataControl.pCon.selectedRaptorName) as GameObject);
        }

        deathSoundEffect = Resources.Load<AudioClip>("Sound effects/player_death_explosion");
    }

    private void Update()
    {
        if (!charSelectUI.activeInHierarchy && !endGameUI.activeInHierarchy)
        {
            timePassed += Time.deltaTime;

            if (timePassed >= 3.0f)
            {
                timePassed = 0;
                PlayerDataControl.pCon.remainingEnergy -= 3;

                updateEnergyBar();

                PlayerDataControl.pCon.score += 30;
                updateScoreDisplay();
            }

            if (PlayerDataControl.pCon.remainingEnergy <= 0)
            {
                Cursor.SetCursor(Resources.Load("textures/cursors/DefaultCursor") as Texture2D, Vector2.zero, CursorMode.Auto);
                FindObjectOfType<CharacterControl>().takingInput = false;
                GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>().velocity = Vector3.zero;
                GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
                Camera.main.gameObject.GetComponent<CameraMover>().camMoveSpeed = 0;
                gameUI.SetActive(false);
                endGameUI.SetActive(true);
                endGameScoreText.GetComponent<Text>().text = PlayerDataControl.pCon.score.ToString();

                if(PlayerDataControl.pCon.score > PlayerDataControl.pCon.highScore)
                {
                    newHighScoreAlert.SetActive(true);
                    PlayerDataControl.pCon.highScore = PlayerDataControl.pCon.score;
                }
            }

            if (PlayerDataControl.pCon.remainingHP <= 0)
            {
                Cursor.SetCursor(Resources.Load("textures/cursors/DefaultCursor") as Texture2D, Vector2.zero, CursorMode.Auto);
                FindObjectOfType<CharacterControl>().takingInput = false;
                GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>().velocity = Vector3.zero;
                GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>().velocity = Vector3.zero;
                GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
                Camera.main.gameObject.GetComponent<CameraMover>().camMoveSpeed = 0;
                GameObject.FindWithTag("Player").GetComponent<AudioSource>().clip = deathSoundEffect;
                GameObject.FindWithTag("Player").GetComponent<AudioSource>().Play();
                StartCoroutine(playDeathAnimation());
                gameUI.SetActive(false);
                endGameUI.SetActive(true);
                endGameScoreText.GetComponent<Text>().text = PlayerDataControl.pCon.score.ToString();

                if (PlayerDataControl.pCon.score > PlayerDataControl.pCon.highScore)
                {
                    newHighScoreAlert.SetActive(true);
                    PlayerDataControl.pCon.highScore = PlayerDataControl.pCon.score;
                }
            }
        }
    }

    public void updateHealthBar()
    {
        RectTransform healthBarRectTransform = healthBar.GetComponent<RectTransform>();
        float remainingHPAsFraction = (float)PlayerDataControl.pCon.remainingHP / (float)PlayerDataControl.pCon.maxHP;
        healthBarRectTransform.sizeDelta = new Vector2(1300.0f * remainingHPAsFraction, 200.0f);
    }

    public void updateEnergyBar()
    {
        RectTransform energyBarRectTransform = energyBar.GetComponent<RectTransform>();
        float remainingEnergyAsFraction = (float)PlayerDataControl.pCon.remainingEnergy / (float)PlayerDataControl.pCon.maxEnergy;
        energyBarRectTransform.sizeDelta = new Vector2(1300.0f * remainingEnergyAsFraction, 200.0f);
    }

    public void updateScoreDisplay()
    {
        scoreText.GetComponent<Text>().text = PlayerDataControl.pCon.score.ToString();
    }

    IEnumerator playDeathAnimation()
    {
        GameObject playerObject = GameObject.FindWithTag("Player");
        SpriteRenderer[] playerSprites = playerObject.GetComponentsInChildren<SpriteRenderer>();

        foreach (SpriteRenderer sprite in playerSprites)
        {
            sprite.enabled = false;
        }

        GameObject explosion = Instantiate<GameObject>(Resources.Load("Prefabs/PlayerExplosion") as GameObject);
        explosion.transform.position = playerObject.transform.position;

        float animationLength = 0f;

        AnimationClip[] clips = explosion.GetComponent<Animator>().runtimeAnimatorController.animationClips;

        for (int i = 0; i < clips.Length; i++)
        {
            if (clips[i].name == "PlayerExplosion")
            {
                animationLength = clips[i].length;
            }
        }

        yield return new WaitForSeconds(animationLength);

        Destroy(explosion);
    }

    public void toStartScreen()
    {
        SceneManager.LoadScene("StartScreen");
        FindObjectOfType<GameSaveLoad>().SaveGame();
    }

    public void exitGame()
    {
        FindObjectOfType<GameSaveLoad>().SaveGame();
        Application.Quit();
    }
}
