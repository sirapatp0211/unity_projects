﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpOrbSpawnControl : MonoBehaviour
{
    GameObject[] powerUpOrbPrefabs;
    public bool gameStarted;
    public float spawnTimer;

    private void Start()
    {
        spawnTimer = 5f;
        powerUpOrbPrefabs = Resources.LoadAll<GameObject>("Prefabs/Power Up Orbs");
    }

    private void Update()
    {
        if (gameStarted)
            spawnTimer -= Time.deltaTime;

        if (spawnTimer <= 0)
        {
            spawnPowerUps();
            spawnTimer = 5f;
        }
    }

    public void spawnPowerUps()
    {
        int numberToSpawn = Random.Range(0, 3);

        for (var i = 0; i < numberToSpawn; i++)
        {
            int spawnID = Random.Range(0, powerUpOrbPrefabs.Length);
            GameObject spawnedOrb = Instantiate<GameObject>(powerUpOrbPrefabs[spawnID]);

            Vector3 screenEdges = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
            Vector3 screenCenter = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 0));

            float maxXPos = (screenEdges.x - screenCenter.x) * 1.9f + screenCenter.x;
            float maxYPos = screenEdges.y * 0.85f;
            float minXPos = (screenEdges.x - screenCenter.x) * 1.3f + screenCenter.x;
            float minYPos = -screenEdges.y * 0.85f;

            float xPos = Random.Range(minXPos, maxXPos);
            float yPos = Random.Range(minYPos, maxYPos);

            spawnedOrb.transform.position = new Vector3(xPos, yPos, -2.5f);
        }
    }
}
