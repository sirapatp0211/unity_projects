﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControl : MonoBehaviour
{
    public bool takingInput;

    public GameObject shotSpawn;

    private Rigidbody2D rBody;
    private Vector2 mousePos;

    public SpriteRenderer primarySprite;
    public SpriteRenderer secondarySprite;
    public SpriteRenderer tertiarySprite;

    private AudioClip singleShotSound;

    void Start()
    {
        rBody = gameObject.GetComponent<Rigidbody2D>();
        shotSpawn = transform.Find("shotSpawn").gameObject;
        takingInput = true;
        transform.SetParent(Camera.main.gameObject.transform);

        primarySprite = transform.Find("primary").GetComponent<SpriteRenderer>();
        secondarySprite = transform.Find("secondary").GetComponent<SpriteRenderer>();
        tertiarySprite = transform.Find("tertiary").GetComponent<SpriteRenderer>();

        singleShotSound = Resources.Load<AudioClip>("Sound effects/player_shot_single");

        Color nullColor = new Color(0, 0, 0, 0);

        if (PlayerDataControl.pCon.primaryColor == nullColor)
            primarySprite.color = PlayerDataControl.pCon.selectedRaptor.defaultPrimary;

        else
            primarySprite.color = PlayerDataControl.pCon.primaryColor;

        if (PlayerDataControl.pCon.secondaryColor == nullColor)
            secondarySprite.color = PlayerDataControl.pCon.selectedRaptor.defaultSecondary;

        else
            secondarySprite.color = PlayerDataControl.pCon.secondaryColor;

        if (PlayerDataControl.pCon.tertiaryColor == nullColor)
            tertiarySprite.color = PlayerDataControl.pCon.selectedRaptor.defaultTertiary;

        else
            tertiarySprite.color = PlayerDataControl.pCon.tertiaryColor;
    }


    void Update()
    {
        if (takingInput)
        {
            if (Input.GetKey(KeyCode.W))
                rBody.velocity = new Vector2(rBody.velocity.x, PlayerDataControl.pCon.spd);

            else if (Input.GetKey(KeyCode.S))
                rBody.velocity = new Vector2(rBody.velocity.x, -PlayerDataControl.pCon.spd);

            if (Input.GetKey(KeyCode.A))
                rBody.velocity = new Vector2(-PlayerDataControl.pCon.spd, rBody.velocity.y);

            else if (Input.GetKey(KeyCode.D))
                rBody.velocity = new Vector2(PlayerDataControl.pCon.spd, rBody.velocity.y);

            float xPos = gameObject.transform.position.x;
            float yPos = gameObject.transform.position.y;

            float topEdge = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height * 0.75f)).y + 0.5f;
            float bottomEdge = Camera.main.ScreenToWorldPoint(new Vector3(0, 0)).y + 0.5f;
            float leftEdge = Camera.main.ScreenToWorldPoint(new Vector3(0, 0)).x + 0.5f;
            float rightEdge = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0)).x - 0.5f;

            xPos = Mathf.Clamp(xPos, leftEdge, rightEdge);
            yPos = Mathf.Clamp(yPos, bottomEdge, topEdge);

            gameObject.transform.position = new Vector2(xPos, yPos);

            mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.gameObject.transform.position.z - 1));

            float dirX = mousePos.x - gameObject.transform.position.x;
            float dirY = mousePos.y - gameObject.transform.position.y;

            Vector2 dir = new Vector2(dirX, dirY);
            dir.Normalize();

            float angle = Mathf.Atan2(dir.y, dir.x);
            angle = angle * Mathf.Rad2Deg;

            gameObject.transform.rotation = Quaternion.Euler(0, 0, angle);

            if (Input.GetMouseButtonDown(0))
            {
                GameObject bullet = Instantiate<GameObject>(Resources.Load("Prefabs/bullet") as GameObject);
                GetComponent<AudioSource>().clip = singleShotSound;
                GetComponent<AudioSource>().Play();

                bullet.transform.position = shotSpawn.transform.position;
                bullet.GetComponent<Rigidbody2D>().velocity = dir * 20;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemy"))
        {
            Physics2D.IgnoreCollision(primarySprite.gameObject.GetComponent<Collider2D>(), collision.gameObject.GetComponent<Collider2D>());
            Physics2D.IgnoreCollision(secondarySprite.gameObject.GetComponent<Collider2D>(), collision.gameObject.GetComponent<Collider2D>());
            Physics2D.IgnoreCollision(tertiarySprite.gameObject.GetComponent<Collider2D>(), collision.gameObject.GetComponent<Collider2D>());
        }
    }
}
