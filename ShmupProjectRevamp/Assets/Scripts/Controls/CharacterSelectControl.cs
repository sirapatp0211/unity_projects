﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelectControl : MonoBehaviour
{
    public Raptor aquila;
    public Raptor haliaeetus;
    public Raptor falco;

    public string selectedRaptorName;

    public GameObject selectionUI;
    public GameObject gameUI;

    public GameObject confirmButton;

    public Image previewImagePrimary;
    public Image previewImageSecondary;
    public Image previewImageTertiary;
    public Text characterNameText;

    public Image aPrimary;
    public Image aSecondary;
    public Image aTertiary;

    public Image hPrimary;
    public Image hSecondary;
    public Image hTertiary;

    public Image fPrimary;
    public Image fSecondary;
    public Image fTertiary;

    public GameObject statsBoard;
    public GameObject statsHeader;
    public Text maxHPText;
    public Text atkText;
    public Text defText;
    public Text spdText;
    public Text energyText;

    private void Start()
    {
        if (PlayerDataControl.pCon.selectedRaptorName != "")
            selectionUI.SetActive(false);
    }

    public void selectRaptor(string raptorName)
    {
        selectedRaptorName = raptorName;
        characterNameText.text = raptorName;
        statsBoard.SetActive(true);
        statsHeader.SetActive(true);
        confirmButton.SetActive(true);
        previewImagePrimary.color = new Color(1, 1, 1, 1);
        previewImageSecondary.color = new Color(1, 1, 1, 1);
        previewImageTertiary.color = new Color(1, 1, 1, 1);

        if(raptorName == "Aquila")
        {
            maxHPText.text = aquila.maxHP.ToString();
            atkText.text = aquila.atk.ToString();
            defText.text = aquila.def.ToString();
            spdText.text = aquila.spd.ToString();
            energyText.text = aquila.energy.ToString();
            previewImagePrimary.sprite = aPrimary.sprite;
            previewImagePrimary.color = aPrimary.color;
            previewImageSecondary.sprite = aSecondary.sprite;
            previewImageSecondary.color = aSecondary.color;
            previewImageTertiary.sprite = aTertiary.sprite;
            previewImageTertiary.color = aTertiary.color;
        }

        else if (raptorName == "Haliaeetus")
        {
            maxHPText.text = haliaeetus.maxHP.ToString();
            atkText.text = haliaeetus.atk.ToString();
            defText.text = haliaeetus.def.ToString();
            spdText.text = haliaeetus.spd.ToString();
            energyText.text = haliaeetus.energy.ToString();
            previewImagePrimary.sprite = hPrimary.sprite;
            previewImagePrimary.color = hPrimary.color;
            previewImageSecondary.sprite = hSecondary.sprite;
            previewImageSecondary.color = hSecondary.color;
            previewImageTertiary.sprite = hTertiary.sprite;
            previewImageTertiary.color = hTertiary.color;
        }

        else if (raptorName == "Falco")
        {
            maxHPText.text = falco.maxHP.ToString();
            atkText.text = falco.atk.ToString();
            defText.text = falco.def.ToString();
            spdText.text = falco.spd.ToString();
            energyText.text = falco.energy.ToString();
            previewImagePrimary.sprite = fPrimary.sprite;
            previewImagePrimary.color = fPrimary.color;
            previewImageSecondary.sprite = fSecondary.sprite;
            previewImageSecondary.color = fSecondary.color;
            previewImageTertiary.sprite = fTertiary.sprite;
            previewImageTertiary.color = fTertiary.color;
        }
    }

    public void confirmSelection()
    {
        PlayerDataControl.pCon.selectedRaptorName = selectedRaptorName;
        PlayerDataControl.pCon.startGame();
        selectionUI.SetActive(false);
        gameUI.SetActive(true);
        Instantiate<GameObject>(Resources.Load("Prefabs/" + PlayerDataControl.pCon.selectedRaptorName) as GameObject);
    }
}
