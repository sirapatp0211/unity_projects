﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class transitionToStart : MonoBehaviour
{
    public Text splashScreenText;
    float timeSinceStart = 0f;

    private void Update()
    {
        timeSinceStart += Time.deltaTime;
        splashScreenText.GetComponent<Text>().color = new Color(1, 1, 1, 1 - timeSinceStart * (1/3f));
        if (timeSinceStart >= 3)
            SceneManager.LoadScene("StartScreen");
    }
}
