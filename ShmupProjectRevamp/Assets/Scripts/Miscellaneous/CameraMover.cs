﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    public float camMoveSpeed;
    public bool gameStarted;

    // Update is called once per frame
    void Update()
    {
        if (gameStarted)
        {
            Vector3 currentPos = gameObject.transform.position;
            gameObject.transform.position = new Vector3(currentPos.x + (camMoveSpeed * Time.deltaTime), currentPos.y, currentPos.z);
        }
    }
}
