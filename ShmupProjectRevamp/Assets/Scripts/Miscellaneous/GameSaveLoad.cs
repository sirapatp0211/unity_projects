﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameSaveLoad : MonoBehaviour
{
    public void SaveGame()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream file;
        if (!File.Exists(Application.persistentDataPath + "/playerData.sav"))
            file = File.Create(Application.persistentDataPath + "/playerData.sav");
        else
            file = File.Open(Application.persistentDataPath + "/playerData.sav", FileMode.Open);

        PlayerData dataToSave = new PlayerData();

        dataToSave.selectedRaptor = PlayerDataControl.pCon.selectedRaptorName;
        dataToSave.highScore = PlayerDataControl.pCon.highScore;

        formatter.Serialize(file, dataToSave);
        file.Close();
    }

    public void LoadGame()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/playerData.sav", FileMode.Open);

        PlayerData loadedData = formatter.Deserialize(file) as PlayerData;

        PlayerDataControl.pCon.selectedRaptorName = loadedData.selectedRaptor;
        PlayerDataControl.pCon.selectedRaptor = Resources.Load<Raptor>("RaptorClasses/" + PlayerDataControl.pCon.selectedRaptorName);
        PlayerDataControl.pCon.highScore = loadedData.highScore;

        file.Close();
    }
}
