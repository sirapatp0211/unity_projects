﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Character Class", menuName = "ScriptableObjects/CharacterClass", order = 1)]
public class Raptor : ScriptableObject
{
    public int maxHP;
    public int atk;
    public int def;
    public int spd;
    public int energy;

    public Color defaultPrimary;
    public Color defaultSecondary;
    public Color defaultTertiary;
}
