﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StageSelectControl : MonoBehaviour
{
    Ray cursorRay;
    RaycastHit rayHit;
    GameObject recentMouseHoverNode;
    string selectedNodeCorrespondingLevel;

    MapNodeData dataToDisplay;
    GUIStyle boxStyle;

    bool clickedOnNode;
    public Image fadeOutImage;

    private void Start()
    {
        clickedOnNode = false;
    }

    private void Update()
    {
        cursorRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(cursorRay.origin, Vector3.forward * 15, new Color(0f, 199 / 255f, 1));
        Physics.Raycast(cursorRay, out rayHit, Mathf.Infinity);

        if (rayHit.collider != null)
        {
            recentMouseHoverNode = rayHit.collider.gameObject;
            print(rayHit.collider.gameObject.name);
            recentMouseHoverNode.GetComponent<Renderer>().material.color = new Color(1f, 191 / 255f, 0);
        }

        else
        {
            if (recentMouseHoverNode != null)
                recentMouseHoverNode.GetComponent<Renderer>().material.color = new Color(0, 120 / 255f, 1f);

            recentMouseHoverNode = null;
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (recentMouseHoverNode != null)
            {
                print("clicked on map node");

                if (recentMouseHoverNode.GetComponent<MapNodeData>().nodeUnlocked)
                {
                    selectedNodeCorrespondingLevel = recentMouseHoverNode.GetComponent<MapNodeData>().correspondingLevel;
                    clickedOnNode = true;
                }
            }
        }

        if (clickedOnNode)
            fadeOutToScene(selectedNodeCorrespondingLevel);
    }

    private void fadeOutToScene(string sceneName)
    {
        if (!fadeOutImage.gameObject.activeInHierarchy)
            fadeOutImage.gameObject.SetActive(true);

        if (fadeOutImage.color.a < 1)
            fadeOutImage.color = new Color(0, 0, 0, fadeOutImage.color.a + (Time.deltaTime / 1.5f));

        if (fadeOutImage.color.a >= 1)
            SceneManager.LoadScene(sceneName);
    }

    private void OnGUI()
    {
        float xScaler = Screen.width / 921f;
        float yScaler = Screen.height / 518f;

        boxStyle = new GUIStyle(GUI.skin.box);
        boxStyle.alignment = TextAnchor.MiddleCenter;
        boxStyle.fontSize = Mathf.RoundToInt(12 * (Screen.width/921f));
        boxStyle.font = Resources.Load("fonts/RaptorCorpsText") as Font;

        if (recentMouseHoverNode != null && !clickedOnNode)
        {
            Vector3 nodePositionInScreenPoint = Camera.main.WorldToScreenPoint(recentMouseHoverNode.gameObject.transform.position);
            Rect boxPos = new Rect(nodePositionInScreenPoint.x - 90 * xScaler, nodePositionInScreenPoint.y + 15 * yScaler, 180 * xScaler, 40 * yScaler);

            dataToDisplay = recentMouseHoverNode.gameObject.GetComponent<MapNodeData>();
            GUI.Box(boxPos, dataToDisplay.levelName, boxStyle);
        }
    }
}
