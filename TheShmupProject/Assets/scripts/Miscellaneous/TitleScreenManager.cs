﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleScreenManager : MonoBehaviour
{
    public GameObject playButtonHolder;
    public GameObject optionsButtonHolder;
    public GameObject exitButtonHolder;

    Image button1Highlight;
    Image button2Highlight;
    Image button3Highlight;

    string selectedButtonName;

    enum AxisState
    {
        idle,
        down,
        held,
        up
    }

    AxisState leftJoystickState;
    AxisState rightJoystickState;

    private void Start()
    {
        leftJoystickState = AxisState.idle;
        rightJoystickState = AxisState.idle;

        button1Highlight = playButtonHolder.transform.Find("Selection Highlight").GetComponent<Image>();
        button2Highlight = optionsButtonHolder.transform.Find("Selection Highlight").GetComponent<Image>();
        button3Highlight = exitButtonHolder.transform.Find("Selection Highlight").GetComponent<Image>();

        button1Highlight.enabled = false;
        button2Highlight.enabled = false;
        button3Highlight.enabled = false;
    }

    private void Update()
    {
        switch (leftJoystickState)
        {
            case AxisState.idle:
                if (Mathf.Abs(Input.GetAxis("Vertical")) > 0.02f)
                    leftJoystickState = AxisState.down;
                break;

            case AxisState.down:
                leftJoystickState = AxisState.held;
                break;

            case AxisState.held:
                if (Mathf.Abs(Input.GetAxis("Vertical")) < 0.02f)
                    leftJoystickState = AxisState.up;
                break;

            case AxisState.up:
                leftJoystickState = AxisState.idle;
                break;
        }

        switch (rightJoystickState)
        {
            case AxisState.idle:
                if (Mathf.Abs(Input.GetAxis("rightStickV")) > 0.02f)
                    rightJoystickState = AxisState.down;
                break;

            case AxisState.down:
                rightJoystickState = AxisState.held;
                break;

            case AxisState.held:
                if (Mathf.Abs(Input.GetAxis("rightStickV")) < 0.02f)
                    rightJoystickState = AxisState.up;
                break;

            case AxisState.up:
                rightJoystickState = AxisState.idle;
                break;
        }

        if((leftJoystickState == AxisState.down || rightJoystickState == AxisState.down))
        {
            if (noButtonSelected())
            {
                button1Highlight.enabled = true;
                selectedButtonName = "Play";
            }

            else
            {
                if (button1Highlight.enabled)
                {
                    if (Input.GetAxis("Vertical") < 0 || Input.GetAxis("rightStickV") < 0)
                    {
                        button1Highlight.enabled = false;
                        button2Highlight.enabled = true;
                        selectedButtonName = "Options";
                    }
                }

                else if (button2Highlight.enabled)
                {
                    if (Input.GetAxis("Vertical") < 0 || Input.GetAxis("rightStickV") < 0)
                    {
                        button2Highlight.enabled = false;
                        button3Highlight.enabled = true;
                        selectedButtonName = "Exit";
                    }
                    else if (Input.GetAxis("Vertical") > 0 || Input.GetAxis("rightStickV") > 0)
                    {
                        button2Highlight.enabled = false;
                        button1Highlight.enabled = true;
                        selectedButtonName = "Play";
                    }
                }

                else if (button3Highlight.enabled)
                {
                    if (Input.GetAxis("Vertical") > 0 || Input.GetAxis("rightStickV") > 0)
                    {
                        button3Highlight.enabled = false;
                        button2Highlight.enabled = true;
                        selectedButtonName = "Options";
                    }
                }
            }
        }
        
        if (Input.GetButton("Submit") && !selectedButtonName.Equals(""))
        {
            if (selectedButtonName.Equals("Play"))
            {
                LoadLevel("save slot selection");
            }

            else if (selectedButtonName.Equals("Options"))
            {
                LoadLevel("options screen");
            }

            else if (selectedButtonName.Equals("Exit"))
                Application.Quit();
        }
    }

    bool noButtonSelected()
    {
        if (!button1Highlight.enabled && !button2Highlight.enabled && !button3Highlight.enabled)
            return true;
        else
            return false;
    }

    public void LoadLevel(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
    }

    public void LoadLevel(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
