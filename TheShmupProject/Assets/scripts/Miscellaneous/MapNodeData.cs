﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapNodeData : MonoBehaviour
{
    public string correspondingLevel;
    public string levelName;
    public bool nodeUnlocked;
}
