﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagePopUpTextSelfDestruct : MonoBehaviour
{
    Vector2 startPos;
    float timeSinceStart;

    private void Start()
    {
        startPos = gameObject.transform.position;
        StartCoroutine(selfDestruct());
        timeSinceStart = 0f;
    }

    private void Update()
    {
        timeSinceStart += Time.deltaTime;
        calculateNewYPos(timeSinceStart);
    }

    IEnumerator selfDestruct()
    {
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }

    void calculateNewYPos(float timePassed)
    {
        float newYPos;

        newYPos = -0.75f * timePassed * (timePassed - 1.25f);

        gameObject.transform.position = new Vector3(gameObject.transform.position.x, startPos.y + newYPos, gameObject.transform.position.z);
    }
}
