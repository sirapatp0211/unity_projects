﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebDemoEnemySpawner : MonoBehaviour
{
    public float timeOfNextSpawnWave;
    public float spawnInterval;
    public float timePassedSinceStart;

    public bool gameStarted;

    private void Start()
    {
        timeOfNextSpawnWave = spawnInterval;
    }

    private void Update()
    {
        if (gameStarted)
            timePassedSinceStart += Time.deltaTime;

        if (timePassedSinceStart >= timeOfNextSpawnWave)
            spawnEnemies();
    }

    private void spawnEnemies()
    {
        int numEnemiesSpawned = Random.Range(1, 6);
        float currentCamXPos = Camera.main.gameObject.transform.position.x;

        for (int i = 0; i < numEnemiesSpawned; i++)
        {
            GameObject spawnedEnemy;

            int spawnedEnemyID = Random.Range(0, 100);

            if (spawnedEnemyID <= 49)
            {
                spawnedEnemy = Instantiate(Resources.Load<GameObject>("prefabs/enemy units/expendable agile fodder"));
                spawnedEnemy.transform.position = new Vector3(currentCamXPos + 2 * FindObjectOfType<CameraMover>().movementSpeed + Random.Range(-3, 3), Random.Range(-4, 5), 2);
            }

            else
            {
                spawnedEnemy = Instantiate(Resources.Load<GameObject>("prefabs/enemy units/expendable fodder"));
                spawnedEnemy.transform.position = new Vector3(currentCamXPos + 2 * FindObjectOfType<CameraMover>().movementSpeed + Random.Range(-3, 3), Random.Range(-4, 5), 2);
            }
        }

        timeOfNextSpawnWave += spawnInterval;
    }
}
