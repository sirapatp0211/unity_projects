﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OptionsScreenManager : MonoBehaviour
{
    public Image mouseKeyboardButtonHighlight;
    public Image gamepadButtonHighlight;

    private void Start()
    {
        mouseKeyboardButtonHighlight = GameObject.Find("mkbButtonHolder").transform.Find("Image").GetComponent<Image>();
        gamepadButtonHighlight = GameObject.Find("gamepadButtonHolder").transform.Find("Image").GetComponent<Image>();

        updateHighlights();
    }

    public void changeMasterVolume()
    {
        int newVolume = Mathf.RoundToInt(GameObject.Find("AudioVolumeArea").transform.Find("MasterVolumeSlider").GetComponent<Slider>().value);
        PlayerPrefs.SetInt("audioVolume", newVolume);
    }

    public void changeControlDeviceTo(string controlDevice)
    {
        PlayerPrefs.SetString("preferredControlDevice", controlDevice);
        updateHighlights();
        ControlsManager.cm.updatePreferredControls();
    }

    void updateHighlights()
    {
        if (PlayerPrefs.GetString("preferredControlDevice").Equals("keyboardAndMouse"))
        {
            mouseKeyboardButtonHighlight.enabled = true;
            gamepadButtonHighlight.enabled = false;
        }

        else if (PlayerPrefs.GetString("preferredControlDevice").Equals("gamepad"))
        {
            mouseKeyboardButtonHighlight.enabled = false;
            gamepadButtonHighlight.enabled = true;
        }

        else
        {
            mouseKeyboardButtonHighlight.enabled = false;
            gamepadButtonHighlight.enabled = false;
        }
    }

    public void returnToTitleScreen()
    {
        SceneManager.LoadScene("title screen");
    }
}
