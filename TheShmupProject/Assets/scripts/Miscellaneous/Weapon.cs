﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon
{
    public string weaponName;
    public Sprite weaponSprite;
    public string flavorText;
    public int atkIncrease;
    public int weaponType;

    public Weapon()
    {
        weaponName = "";
        weaponSprite = null;
        flavorText = "";
        atkIncrease = 0;
        weaponType = 0;
    }
}
