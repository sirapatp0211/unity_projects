﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Vector2 dir;

    public bool isUpperWave;
    public bool isLowerWave;

    public float timeSinceInstantiation;
    public int damageToDeal;
    public GameObject damagePopUp;

    private void Start()
    {
        Physics2D.IgnoreLayerCollision(8, 8);

        GetComponent<Rigidbody2D>().velocity = dir;
        GetComponent<Rigidbody2D>().drag = 0;
        GetComponent<Rigidbody2D>().gravityScale = 0;

        gameObject.transform.SetParent(Camera.main.transform);
    }

    private void Update()
    {
        if (!this.GetComponent<Renderer>().isVisible)
            Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemy"))
        {
            damagePopUp = Instantiate<GameObject>(Resources.Load("prefabs/DamagePopUpText") as GameObject);
            damagePopUp.GetComponent<TextMesh>().text = damageToDeal.ToString();
            Vector2 impactPoint = gameObject.transform.position;
            damagePopUp.transform.position = new Vector3(impactPoint.x, impactPoint.y, collision.transform.position.z - 1);

            collision.gameObject.GetComponent<EnemyController>().remainingHealth -= damageToDeal;
        }

        else if (collision.gameObject.tag.Equals("Enemy Boss"))
        {
            //collision.gameObject.GetComponent<EnemyBossController>().remainingHealth -= Mathf.RoundToInt(damageToDeal * collision.gameObject.GetComponent<EnemyBossController>().incomingDmgMultiplier);
        }

        Destroy(gameObject);
    }
}
