﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClassSelectControl : MonoBehaviour
{
    public GameObject characterSelectionUI;
    public Text className;
    public Text flavorText;
    public Image previewImage_Primary;
    public Image previewImage_Secondary;
    public Image previewImage_Tertiary;

    public GameObject offenseStatIndicator;
    public GameObject durabilityStatIndicator;
    public GameObject speedStatIndicator;

    float cellSizeX;
    float cellSizeY;

    public int selectionID;

    private void Start()
    {
        selectionID = 1;
        updateSelectionDisplay();
    }

    public void setClass()
    {
        for (int i = 1; i <= 3; i++)
        {
            if (((GameControl.playerClass)i).ToString().Equals(className.text))
                GameControl.control.selectedClass = (GameControl.playerClass)i;
        }
        closeUI();
        FindObjectOfType<Level1_1DialogueUIControl>().ReadTextFromNextLine();
    }

    public void rotateSelection(int direction)
    {
        selectionID += direction;
        updateSelectionDisplay();
    }

    public void closeUI()
    {
        characterSelectionUI.SetActive(false);
    }

    void updateSelectionDisplay()
    {
        if (findRemainder(selectionID, 3) == 1)
        {
            className.text = "Aquila";
            previewImage_Primary.sprite = Resources.Load<Sprite>("textures/Characters/Player/Aquila/PrimaryGrayScale");
            previewImage_Primary.color = new Color(156 / 255f, 109 / 255f, 48 / 255f);

            previewImage_Secondary.sprite = Resources.Load<Sprite>("textures/Characters/Player/Aquila/SecondaryGrayScale");
            previewImage_Secondary.color = new Color(1, 195 / 5f, 1 / 255f);

            previewImage_Tertiary.sprite = Resources.Load<Sprite>("textures/Characters/Player/Aquila/TertiaryGrayScale");
            previewImage_Tertiary.color = new Color(126 / 255f, 178 / 255f, 1);

            flavorText.text = "Annihilate the enemy with this beast of a jet.";

            updateStatDisplayBoard("Aquila");
        }

        else if (findRemainder(selectionID, 3) == 2)
        {
            className.text = "Haliaeetus";
            previewImage_Primary.sprite = Resources.Load<Sprite>("textures/Characters/Player/Haliaeetus/PrimaryGrayScale");
            previewImage_Primary.color = new Color(87 / 255f, 60 / 255f, 12 / 255f);

            previewImage_Secondary.sprite = Resources.Load<Sprite>("textures/Characters/Player/Haliaeetus/SecondaryGrayScale");
            previewImage_Secondary.color = new Color(1, 173 / 255f, 31 / 255f);

            previewImage_Tertiary.sprite = Resources.Load<Sprite>("textures/Characters/Player/Haliaeetus/TertiaryGrayScale");
            previewImage_Tertiary.color = Color.white;

            flavorText.text = "This jet-submarine hybrid machine is nimble both in the air and in the water.";

            updateStatDisplayBoard("Haliaeetus");
        }

        else if (findRemainder(selectionID, 3) == 0)
        {
            className.text = "Falco";
            previewImage_Primary.sprite = Resources.Load<Sprite>("textures/Characters/Player/Falco/PrimaryGrayScale");
            previewImage_Primary.color = new Color(108 / 255f, 88 / 255f, 74 / 255f);

            previewImage_Secondary.sprite = Resources.Load<Sprite>("textures/Characters/Player/Falco/SecondaryGrayScale");
            previewImage_Secondary.color = new Color(241 / 255f, 185 / 255f, 58 / 255f);

            previewImage_Tertiary.sprite = Resources.Load<Sprite>("textures/Characters/Player/Falco/TertiaryGrayScale");
            previewImage_Tertiary.color = new Color(138 / 255f, 229 / 255f, 233 / 255f);


            flavorText.text = "No aircraft can match the agility of this fighter jet.";

            updateStatDisplayBoard("Falco");
        }
    }

    int findRemainder(int dividend, int divisor)
    {
        if (dividend > 0)
            return (dividend % divisor);

        else
        {
            float quotient = (float)dividend / divisor;
            float modulo = quotient - Mathf.Ceil(quotient);
            int result = Mathf.RoundToInt(modulo * divisor);
            int remainder = divisor + result;
            if (remainder == divisor)
                remainder = 0;
            return remainder;
        }
    }

    void updateStatDisplayBoard(string className)
    {
        clearStatsBoard();

        if (className.Equals("Aquila"))
            setStatIndicators(5, 5, 1);

        else if (className.Equals("Haliaeetus"))
            setStatIndicators(4, 4, 3);

        else if (className.Equals("Falco"))
            setStatIndicators(3, 2, 5);
    }

    void clearStatsBoard()
    {
        for (int i = 0; i < 5; i++)
        {
            offenseStatIndicator.transform.GetChild(i).gameObject.SetActive(false);
            durabilityStatIndicator.transform.GetChild(i).gameObject.SetActive(false);
            speedStatIndicator.transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    void setStatIndicators(int offense, int durability, int speed)
    {
        for (int o = 0; o < offense; o++)
        {
            offenseStatIndicator.transform.GetChild(o).gameObject.SetActive(true);

            if (o == 0)
                offenseStatIndicator.transform.GetChild(o).GetComponent<Image>().color = Color.red;

            else if (o < 3)
                offenseStatIndicator.transform.GetChild(o).GetComponent<Image>().color = Color.yellow;

            else
                offenseStatIndicator.transform.GetChild(o).GetComponent<Image>().color = Color.green;
        }

        for (int d = 0; d < durability; d++)
        {
            durabilityStatIndicator.transform.GetChild(d).gameObject.SetActive(true);

            if (d == 0)
                durabilityStatIndicator.transform.GetChild(d).GetComponent<Image>().color = Color.red;

            else if (d < 3)
                durabilityStatIndicator.transform.GetChild(d).GetComponent<Image>().color = Color.yellow;

            else
                durabilityStatIndicator.transform.GetChild(d).GetComponent<Image>().color = Color.green;
        }

        for (int s = 0; s < speed; s++)
        {
            speedStatIndicator.transform.GetChild(s).gameObject.SetActive(true);

            if (s == 0)
                speedStatIndicator.transform.GetChild(s).GetComponent<Image>().color = Color.red;

            else if (s < 3)
                speedStatIndicator.transform.GetChild(s).GetComponent<Image>().color = Color.yellow;

            else
                speedStatIndicator.transform.GetChild(s).GetComponent<Image>().color = Color.green;
        }
    }
}
