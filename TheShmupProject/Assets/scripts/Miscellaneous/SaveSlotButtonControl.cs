﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveSlotButtonControl : MonoBehaviour
{
    public int slotNum;
    public GameObject emptyTextContainer;
    public GameObject occupiedButtonContainer;
    Text nameText;
    Text classText;

    private void Start()
    {
        if (SaveAndLoad.loadDataFromSlot(slotNum) != null)
        {
            emptyTextContainer.SetActive(false);
            occupiedButtonContainer.SetActive(true);

            nameText = occupiedButtonContainer.transform.Find("PlayerName").GetComponent<Text>();
            classText = occupiedButtonContainer.transform.Find("PlayerClass").GetComponent<Text>();

            PlayerData containedData = (PlayerData)SaveAndLoad.loadDataFromSlot(slotNum);
            nameText.text = "Name: " + containedData.playerName;

            if (containedData.selectedClass == 1)
                classText.text = "Aquila";

            else if (containedData.selectedClass == 2)
                classText.text = "Haliaeetus";

            else if (containedData.selectedClass == 3)
                classText.text = "Buteo";
        }

        else
        {
            emptyTextContainer.SetActive(true);
            occupiedButtonContainer.SetActive(false);
        }
    }
}
