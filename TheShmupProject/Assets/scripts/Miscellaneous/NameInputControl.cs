﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NameInputControl : MonoBehaviour
{
    public GameObject nameInputBox;
    public GameObject confirmationDialogueBox;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return) && nameInputBox.activeInHierarchy)
            setPlayerName();
    }

    public void setPlayerName()
    {
        GameControl.control.playerName = nameInputBox.transform.Find("Name input field").GetComponent<InputField>().text;
        nameInputBox.SetActive(false);
        confirmationDialogueBox.SetActive(true);
        confirmationDialogueBox.transform.Find("ConfirmationText").GetComponent<Text>().text = "Use the name " + GameControl.control.playerName + "?";
    }

    public void reopenNameInputBox()
    {
        nameInputBox.SetActive(true);
        confirmationDialogueBox.SetActive(false);
    }

    public void endInputProcess()
    {
        confirmationDialogueBox.SetActive(false);
        FindObjectOfType<DialogueUIControl>().DialogueBoxCanvas.SetActive(true);
        FindObjectOfType<DialogueUIControl>().ReadTextFromNextLine();
    }
}
