﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControlToggler : MonoBehaviour
{
    public bool gameStarted;
    float timeToStart;
    int timeToStartToNearestSecond;

    private void Start()
    {
        FindObjectOfType<PlayerController>().enabled = false;
        FindObjectOfType<CameraMover>().enabled = false;
        timeToStart = 3f;
    }

    private void Update()
    {
        if (gameStarted)
        {
            StartCoroutine(startGame());
            timeToStart -= Time.deltaTime;
        }

        timeToStartToNearestSecond = Mathf.CeilToInt(timeToStart);

        if (timeToStartToNearestSecond < 1)
            FindObjectOfType<webDemoControl>().countdownCanvas.GetComponentInChildren<Image>().enabled = false;

        FindObjectOfType<webDemoControl>().countdownCanvas.GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("textures/UI Objects/CountdownTimerIndicator" + timeToStartToNearestSecond.ToString());
    }

    IEnumerator startGame()
    {
        yield return new WaitForSeconds(3);
        FindObjectOfType<PlayerController>().enabled = true;
        FindObjectOfType<CameraMover>().enabled = true;
        FindObjectOfType<webDemoControl>().countdownCanvas.SetActive(false);
        FindObjectOfType<webDemoControl>().inGameCanvas.SetActive(true);
    }
}
