﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    public float movementSpeed;

    public float finalXPos;

    private void Start()
    {
        if (finalXPos == 0)
            finalXPos = Mathf.Infinity;
    }

    private void FixedUpdate()
    {
        if(FindObjectOfType<PlayerController>() != null)
        {
            if (FindObjectOfType<PlayerController>().gameStarting)
            {
                Vector3 pos = gameObject.transform.position;

                if (pos.x < finalXPos)
                    pos = new Vector3(pos.x + movementSpeed * Time.deltaTime, pos.y, pos.z);

                else
                    pos = new Vector3(finalXPos, pos.y, pos.z);

                gameObject.transform.position = pos;
            }
        }

        if(gameObject.transform.position.x == finalXPos && !FindObjectOfType<GameLevelControl>().levelEnded)
        {
            if (FindObjectOfType<GameLevelControl>().levelMode == GameLevelControl.gameMode.Rush)
                FindObjectOfType<GameLevelControl>().EndLevel(0);
        }
    }
}
