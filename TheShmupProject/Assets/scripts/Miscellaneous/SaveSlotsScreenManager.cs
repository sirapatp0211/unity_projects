﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveSlotsScreenManager : MonoBehaviour
{
    public GameObject slot1Button;
    public GameObject slot2Button;
    public GameObject slot3Button;

    Image slot1Highlight;
    Image slot2Highlight;
    Image slot3Highlight;

    bool slot1Selected;
    bool slot2Selected;
    bool slot3Selected;

    enum AxisState
    {
        idle,
        down,
        held,
        up
    }

    AxisState leftJoystickState;
    AxisState rightJoystickState;

    private void Start()
    {
        leftJoystickState = AxisState.idle;
        rightJoystickState = AxisState.idle;

        slot1Highlight = slot1Button.transform.Find("highlight").GetComponent<Image>();
        slot2Highlight = slot2Button.transform.Find("highlight").GetComponent<Image>();
        slot3Highlight = slot3Button.transform.Find("highlight").GetComponent<Image>();

        slot1Highlight.enabled = false;
        slot2Highlight.enabled = false;
        slot3Highlight.enabled = false;

        slot1Selected = false;
        slot2Selected = false;
        slot3Selected = false;
    }

    public void Update()
    {
        switch (leftJoystickState)
        {
            case AxisState.idle:
                if (Mathf.Abs(Input.GetAxis("Vertical")) > 0.02f)
                    leftJoystickState = AxisState.down;
                break;

            case AxisState.down:
                leftJoystickState = AxisState.held;
                break;

            case AxisState.held:
                if (Mathf.Abs(Input.GetAxis("Vertical")) < 0.02f)
                    leftJoystickState = AxisState.up;
                break;

            case AxisState.up:
                leftJoystickState = AxisState.idle;
                break;
        }

        switch (rightJoystickState)
        {
            case AxisState.idle:
                if (Mathf.Abs(Input.GetAxis("rightStickV")) > 0.02f)
                    rightJoystickState = AxisState.down;
                break;

            case AxisState.down:
                rightJoystickState = AxisState.held;
                break;

            case AxisState.held:
                if (Mathf.Abs(Input.GetAxis("rightStickV")) < 0.02f)
                    rightJoystickState = AxisState.up;
                break;

            case AxisState.up:
                rightJoystickState = AxisState.idle;
                break;
        }

        if (leftJoystickState == AxisState.down || rightJoystickState == AxisState.down)
        {
            if (noSlotSelected())
            {
                slot1Highlight.enabled = true;
                slot1Selected = true;
                GameControl.control.selectedSlot = 1;
            }

            else
            {
                if (slot1Selected)
                {
                    if(Input.GetAxis("Vertical") < -0.02f || Input.GetAxis("rightStickV") < -0.02f)
                    {
                        slot1Selected = false;
                        slot1Highlight.enabled = false;

                        slot2Selected = true;
                        slot2Highlight.enabled = true;
                        GameControl.control.selectedSlot = 2;
                    }
                }

                else if (slot2Selected)
                {
                    if (Input.GetAxis("Vertical") < -0.02f || Input.GetAxis("rightStickV") < -0.02f)
                    {
                        slot1Selected = false;
                        slot1Highlight.enabled = false;

                        slot2Selected = false;
                        slot2Highlight.enabled = false;

                        slot3Selected = true;
                        slot3Highlight.enabled = true;
                        GameControl.control.selectedSlot = 3;
                    }

                    else if (Input.GetAxis("Vertical") > 0.02f || Input.GetAxis("rightStickV") > 0.02f)
                    {
                        slot1Selected = true;
                        slot1Highlight.enabled = true;
                        GameControl.control.selectedSlot = 1;

                        slot2Selected = false;
                        slot2Highlight.enabled = false;

                        slot3Selected = false;
                        slot3Highlight.enabled = false;
                    }
                }

                else if (slot3Selected)
                {
                    if (Input.GetAxis("Vertical") > 0.02f || Input.GetAxis("rightStickV") > 0.02f)
                    {
                        slot2Selected = true;
                        slot2Highlight.enabled = true;
                        GameControl.control.selectedSlot = 2;

                        slot3Selected = false;
                        slot3Highlight.enabled = false;
                    }
                }
            }
        }

        if (Input.GetButton("Submit"))
        {
            if (slot1Selected)
                GameControl.control.CheckSlot(1);

            else if (slot2Selected)
                GameControl.control.CheckSlot(2);

            else if (slot3Selected)
                GameControl.control.CheckSlot(3);

            else
            {

            }
        }
    }

    bool noSlotSelected()
    {
        if (!slot1Selected && !slot2Selected && !slot3Selected)
            return true;

        else
            return false;
    }
}
