﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveAndLoad
{
    public static void createSaveFile(int slot)
    {
        File.Create(Application.persistentDataPath + "/saveFile" + slot + ".trc");
    }

    public static void Save(int slot, PlayerData data)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/saveFile" + slot + ".trc");
        formatter.Serialize(file, data);
        file.Close();
    }

    public static PlayerData loadDataFromSlot(int slot)
    {
        if (File.Exists(Application.persistentDataPath + "/saveFile" + slot + ".trc"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/saveFile" + slot + ".trc", FileMode.Open);
            PlayerData loadedData = (PlayerData)formatter.Deserialize(file);
            file.Close();
            return loadedData;
        }
        return null;
    }
}
