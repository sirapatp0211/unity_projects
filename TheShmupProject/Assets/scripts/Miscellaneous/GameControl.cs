﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour
{
    public static GameControl control;

    public bool isFirstPlaySession;

    public int selectedSlot;

    public int selectedDifficulty;
    // 0 = easy
    // 1 = normal
    // 2 = hard

    public string playerName;

    public enum playerClass
    {
        Aquila = 1,
        Haliaeetus = 2,
        Falco = 3
    }

    public playerClass selectedClass;

    public int atkDamage;
    public int maxHealth;

    public Weapon slot1EquippedFirearm;
    public Weapon slot2EquippedFirearm;

    public List<string> completedLevels;
    public List<Weapon> playerArsenal;

    private void Start()
    {
        if (control == null)
        {
            control = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    public void SaveGameToSlot(int slot)
    {
        PlayerData dataToSave = new PlayerData();

        dataToSave.playerName = control.playerName;

        dataToSave.difficulty = control.selectedDifficulty;

        dataToSave.selectedClass = (int)control.selectedClass;

        dataToSave.slot1EquippedFirearm = control.slot1EquippedFirearm;
        dataToSave.slot2EquippedFirearm = control.slot2EquippedFirearm;

        dataToSave.completedLevelIDs = control.completedLevels;
        dataToSave.playerWeaponInventory = control.playerArsenal;

        SaveAndLoad.Save(slot, dataToSave);
    }

    public void LoadGameFromSlot(int slot)
    {
        PlayerData loadedData = SaveAndLoad.loadDataFromSlot(slot);

        control.playerName = loadedData.playerName;

        control.selectedDifficulty = loadedData.difficulty;

        control.selectedClass = (playerClass)loadedData.selectedClass;

        control.slot1EquippedFirearm = loadedData.slot1EquippedFirearm;
        control.slot2EquippedFirearm = loadedData.slot2EquippedFirearm;

        control.completedLevels = loadedData.completedLevelIDs;
        control.playerArsenal = loadedData.playerWeaponInventory;

        control.isFirstPlaySession = false;

        SceneManager.LoadScene("stage selection");
    }

    public void selectSlot(int slot)
    {
        print("selected slot: " + slot);
    }

    public void CheckSlot(int slot)
    {
        if (SaveAndLoad.loadDataFromSlot(slot) != null)
            LoadGameFromSlot(slot);

        else
        {
            control.isFirstPlaySession = true;
            SceneManager.LoadScene("stage selection");
        }
    }

    public void setPlaceholders()
    {
        control.completedLevels = new List<string>();
    }
}
