﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level1_1DialogueUIControl : DialogueUIControl
{
    private void Start()
    {
        if(!GameControl.control.completedLevels.Contains("1-1"))
        {
            Time.timeScale = 0;
            initializeVariables();
            setReferenceTextTo("Level1_1RefTxt");
            ReadTextFromLine(0);
        }
    }
}
