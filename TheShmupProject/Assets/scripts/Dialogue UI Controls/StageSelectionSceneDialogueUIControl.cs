﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageSelectionSceneDialogueUIControl : DialogueUIControl
{ 
    private void Start()
    {
        initializeVariables();
        setReferenceTextTo("Col_McHawkface_Intro");
        ReadTextFromLine(0);
    }
}
