﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueUIControl : MonoBehaviour
{
    public GameObject DialogueBoxCanvas;
    Text CharacterName;
    Text CharacterSpeech;
    Image CharacterIcon;

    public GameObject nameInputBox;
    public GameObject nameConfirmDialogueBox;

    public GameObject characterChoiceBox;

    TextAsset referenceText;
    int currentTextLine;
    string[] dialogueLines;
    string[] dialogueLineParts;

    public void initializeVariables()
    {
        nameInputBox = GameObject.Find("Name input box");
        if (nameInputBox != null)
            nameInputBox.SetActive(false);

        nameConfirmDialogueBox = GameObject.Find("Confirmation box");
        if (nameConfirmDialogueBox != null)
            nameConfirmDialogueBox.SetActive(false);

        characterChoiceBox = GameObject.Find("character choice UI");
        if (characterChoiceBox != null)
            characterChoiceBox.SetActive(false);

        DialogueBoxCanvas = GameObject.Find("Dialogue UI");
        CharacterName = DialogueBoxCanvas.transform.Find("Character Name Frame").transform.Find("Character Name Text").GetComponent<Text>();
        CharacterSpeech = DialogueBoxCanvas.transform.Find("Character Speech Box Frame").transform.Find("Character Speech Box").GetComponent<Text>();
        CharacterIcon = DialogueBoxCanvas.transform.Find("Character Image Frame").transform.Find("Character Image").GetComponent<Image>();
        currentTextLine = 0;
    }

    public void ReadTextFromLine(int line)
    {
        dialogueLineParts = dialogueLines[line].Split('|');
        string trimmedPart1 = dialogueLineParts[0].Trim();
        string trimmedPart2 = dialogueLineParts[1].Trim();

        if (trimmedPart1.Equals("Action"))
        {
            if (trimmedPart2.Equals("GetPlayerName"))
            {
                nameInputBox.SetActive(true);
                DialogueBoxCanvas.SetActive(false);
            }

            if (trimmedPart2.Equals("CharacterSelect"))
            {
                characterChoiceBox.SetActive(true);
            }

            if (trimmedPart2.Equals("StartLevel"))
            {
                Time.timeScale = 1;
                FindObjectOfType<GameLevelControl>().timerDisplay.SetActive(true);
                FindObjectOfType<GameLevelControl>().StartLevel();
                DialogueBoxCanvas.SetActive(false);
            }
        }

        else
        {
            if (trimmedPart1.Equals("PlayerName"))
                CharacterName.text = GameControl.control.playerName;
            else
                CharacterName.text = trimmedPart1;

            if (trimmedPart2.Contains("PlayerName"))
                trimmedPart2 = trimmedPart2.Replace("PlayerName", GameControl.control.playerName);

            CharacterSpeech.text = trimmedPart2;
        }
    }

    public void ReadTextFromNextLine()
    {
        print("clicked on continue button");
        currentTextLine++;
        if (currentTextLine < dialogueLines.Length)
            ReadTextFromLine(currentTextLine);
        else
            DialogueBoxCanvas.SetActive(false);
    }

    public void setReferenceTextTo(string referenceName)
    {
        referenceText = Resources.Load("reference texts/" + referenceName) as TextAsset;
        dialogueLines = referenceText.text.Split('\n');
        currentTextLine = 0;
    }
}
