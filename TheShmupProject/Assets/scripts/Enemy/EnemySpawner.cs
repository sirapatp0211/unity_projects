﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public void spawnEnemy(string enemyName, float xMin, float xMax, float yMin, float yMax)
    {
        GameObject spawnedEnemy = Instantiate(Resources.Load<GameObject>("prefabs/enemy units/" + enemyName));

        float enemyXPos = Random.Range(xMin, xMax);
        float enemyYPos = Random.Range(yMin, yMax);

        spawnedEnemy.transform.position = new Vector3(enemyXPos, enemyYPos, spawnedEnemy.transform.position.z);
    }
}
