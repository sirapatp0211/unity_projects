﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyController : MonoBehaviour
{
    public Vector2 shotSpawn;
    GameObject shotSpawnObject;

    public Enemy enemyInfo;

    public float timeSinceStart;

    public int maxHealth;
    public int remainingHealth;

    public int atkDmg;

    public bool shootingUp;
    public bool shootingDown;
    public bool shootingLeft;
    public bool shootingRight;
    public bool shootingWithRotation;
    public bool turnToPlayer;

    public float fireRate;
    float timeToNextShot;
    float timeSinceFirstShot;

    bool enteredScene;

    private void Start()
    {
        if (GetComponent<Renderer>().isVisible)
            enteredScene = true;

        else
            enteredScene = false;

        maxHealth = enemyInfo.maxHealth;
        remainingHealth = maxHealth;
        atkDmg = enemyInfo.damageToDeal;
        fireRate = enemyInfo.fireRate;
        
        shootingUp = enemyInfo.shootUpward;
        shootingDown = enemyInfo.shootDownward;
        shootingLeft = enemyInfo.shootLeftward;
        shootingRight = enemyInfo.shootRightward;
        shootingWithRotation = enemyInfo.shootWithRotation;
        turnToPlayer = enemyInfo.rotateToFacePlayer;

        shotSpawnObject = new GameObject();
        setShotSpawn();
        timeSinceStart = 0.00f;
        timeToNextShot = 0.00f;
    }

    private void Update()
    {
        if (GetComponent<Renderer>().isVisible)
            enteredScene = true;

        if (remainingHealth <= 0)
            Die();

        shotSpawn = (Vector2)shotSpawnObject.transform.position;

        if (timeSinceFirstShot >= timeToNextShot)
            shoot();

        if (enteredScene)
        {
            if (!GetComponent<Renderer>().isVisible)
                Destroy(gameObject);
        }

        timeSinceFirstShot += Time.deltaTime;
        timeSinceStart += Time.deltaTime;
    }

    public void setShotSpawn()
    {
        Vector3 gameObjectPosition = gameObject.transform.position;
        Vector3 gameObjectRotation = gameObject.transform.rotation.eulerAngles;

        if (shootingUp)
        {
            shotSpawnObject.transform.position = new Vector3(gameObjectPosition.x, gameObjectPosition.y + 1, gameObjectPosition.z);
            shotSpawn = (Vector2)shotSpawnObject.transform.position;
            shotSpawnObject.transform.SetParent(gameObject.transform);
        }

        else if (shootingDown)
        {
            shotSpawnObject.transform.position = new Vector3(gameObjectPosition.x, gameObjectPosition.y - 1, gameObjectPosition.z);
            shotSpawn = (Vector2)shotSpawnObject.transform.position;
            shotSpawnObject.transform.SetParent(gameObject.transform);
        }

        else if (shootingLeft)
        {
            shotSpawnObject.transform.position = new Vector3(gameObjectPosition.x - 1, gameObjectPosition.y, gameObjectPosition.z);
            shotSpawn = (Vector2)shotSpawnObject.transform.position;
            shotSpawnObject.transform.SetParent(gameObject.transform);
        }

        else if (shootingRight)
        {
            shotSpawnObject.transform.position = new Vector3(gameObjectPosition.x + 1, gameObjectPosition.y, gameObjectPosition.z);
            shotSpawn = (Vector2)shotSpawnObject.transform.position;
            shotSpawnObject.transform.SetParent(gameObject.transform);
        }

        else if (shootingWithRotation)
        {
            float shotSpawnXOffset = 0;
            float shotSpawnYOffset = 0;

            shotSpawnYOffset = Mathf.Sin(gameObjectRotation.z * Mathf.Deg2Rad);
            shotSpawnXOffset = Mathf.Cos(gameObjectRotation.z * Mathf.Deg2Rad);

            shotSpawnObject.transform.position = new Vector3(gameObjectPosition.x + shotSpawnXOffset, gameObjectPosition.y + shotSpawnYOffset, gameObjectPosition.z);
            shotSpawn = (Vector2)shotSpawnObject.transform.position;
            shotSpawnObject.transform.SetParent(gameObject.transform);

            if (turnToPlayer)
                gameObject.AddComponent<EnemyRotator>();
        }
    }

    private void shoot()
    {
        GameObject bullet = Instantiate(Resources.Load<GameObject>("prefabs/enemyBullet"), (Vector3)shotSpawn, Quaternion.identity);
        bullet.GetComponent<EnemyBullet>().damage = atkDmg;

        if (shootingUp)
            bullet.GetComponent<Rigidbody2D>().velocity = Vector2.up * 20;

        else if (shootingDown)
            bullet.GetComponent<Rigidbody2D>().velocity = Vector2.down * 20;

        else if (shootingLeft)
            bullet.GetComponent<Rigidbody2D>().velocity = Vector2.left * 20;

        else if (shootingRight)
            bullet.GetComponent<Rigidbody2D>().velocity = Vector2.right * 20;

        else if (shootingWithRotation)
        {
            Vector2 dir = (Vector2)shotSpawn - (Vector2)gameObject.transform.position;
            bullet.GetComponent<Rigidbody2D>().velocity = dir * 20;
        }

        timeToNextShot += (1f / fireRate);
    }

    void Die()
    {
        if (SceneManager.GetActiveScene().name.Contains("Web"))
        {
            FindObjectOfType<webDemoControl>().score += 20;
            FindObjectOfType<webDemoControl>().updateScoreDisplay();
        }

        Destroy(gameObject);
    }
}
