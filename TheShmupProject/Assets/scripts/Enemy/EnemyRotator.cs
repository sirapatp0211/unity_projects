﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRotator : MonoBehaviour
{
    public Transform playerTransform;

    Vector2 directionToPlayer;
    float rotationAngle;

    private void Start()
    {
        playerTransform = GameObject.FindWithTag("Player").transform;
    }

    private void FixedUpdate()
    {
        directionToPlayer = playerTransform.position - gameObject.transform.position;
        directionToPlayer.Normalize();
        rotationAngle = Mathf.Atan2(directionToPlayer.y, directionToPlayer.x);

        Vector3 currentRotation = gameObject.transform.rotation.eulerAngles;

        gameObject.GetComponent<Transform>().eulerAngles = new Vector3(currentRotation.x, currentRotation.y, rotationAngle * Mathf.Rad2Deg);
    }
}
