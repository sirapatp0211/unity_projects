﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMover : MonoBehaviour
{
    public Enemy enemyInfo;

    public bool movingInSineWave;
    public float sinusoidWaveFrequency;
    public float sinusoidWaveHeight;

    public bool movingDiagonally;
    public float movementSlope;

    public bool moveWithCamera;
    public float xCoordinateOnScreenToStartSync;

    float timeSinceStart;

    private void Start()
    {
        GetComponent<Rigidbody2D>().gravityScale = 0;
        GetComponent<Rigidbody2D>().drag = 0;
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        Physics2D.IgnoreLayerCollision(10, 10);

        timeSinceStart = 0;

        movingInSineWave = enemyInfo.sineWaveMovement;
        sinusoidWaveFrequency = enemyInfo.waveFrequency;
        sinusoidWaveHeight = enemyInfo.waveHeight;

        movingDiagonally = enemyInfo.diagonalMovement;
        movementSlope = enemyInfo.diagonalSlope;

        moveWithCamera = enemyInfo.syncMovementWithCamera;
        xCoordinateOnScreenToStartSync = enemyInfo.horizontalPosToSync;
    }

    private void FixedUpdate()
    {
        timeSinceStart += Time.deltaTime;

        if (movingInSineWave)
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, sinusoidWaveHeight * Mathf.Cos(timeSinceStart * sinusoidWaveFrequency));

        else if (movingDiagonally)
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, -movementSlope * Camera.main.GetComponent<CameraMover>().movementSpeed);

        else if (moveWithCamera && Camera.main.WorldToScreenPoint(gameObject.transform.position).x <= xCoordinateOnScreenToStartSync * Screen.width && gameObject.transform.parent == null)
            gameObject.transform.SetParent(Camera.main.transform);


        else
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
    }
}
