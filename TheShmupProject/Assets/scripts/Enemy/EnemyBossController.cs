﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBossController : MonoBehaviour
{
    public EnemyBoss bossInfo;

    public float incomingDmgMultiplier;

    public int maxHealth;
    public int remainingHealth;
}
