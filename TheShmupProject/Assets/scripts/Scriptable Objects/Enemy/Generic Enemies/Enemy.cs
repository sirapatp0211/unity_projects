﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "enemy", menuName = "Enemy Info", order = 1)]
public class Enemy : ScriptableObject
{
    public int maxHealth;
    public float fireRate;

    public int damageToDeal;

    public bool shootUpward;
    public bool shootDownward;
    public bool shootLeftward;
    public bool shootRightward;
    public bool shootWithRotation;
    public bool rotateToFacePlayer;

    public bool sineWaveMovement;
    public float waveFrequency;
    public float waveHeight;

    public bool diagonalMovement;
    public float diagonalSlope;

    public bool syncMovementWithCamera;
    public float horizontalPosToSync;
}
