﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BossInfo", menuName = "Boss Info", order = 1)]
public class EnemyBoss : ScriptableObject
{
    public string bossName;
    public float incomingDamageMultiplier;

    public int maxHealth;
    public int remainingHealth;
}
