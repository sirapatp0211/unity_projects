﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlsManager : MonoBehaviour
{
    public static ControlsManager cm;
    public bool isUsingMouse;

    void Start()
    {
        if (cm == null)
        {
            cm = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);

        updatePreferredControls();
    }

    public void updatePreferredControls()
    {
        if (PlayerPrefs.GetString("preferredControlDevice").Equals("keyboardAndMouse"))
            isUsingMouse = true;
        else
            isUsingMouse = false;
    }

    public void SetPreferredControlDevice(string preferredDeviceName)
    {
        PlayerPrefs.SetString("preferredControlDevice", preferredDeviceName);
    }
}
