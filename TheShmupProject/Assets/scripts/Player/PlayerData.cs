﻿using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PlayerData
{
    public string playerName;

    public int difficulty;

    public int selectedClass;

    public Weapon slot1EquippedFirearm;
    public Weapon slot2EquippedFirearm;

    public List<Weapon> playerWeaponInventory;

    public List<string> completedLevelIDs;

    public PlayerData()
    {
        playerName = "";

        difficulty = 0;

        selectedClass = 0;

        slot1EquippedFirearm = new Weapon();
        slot2EquippedFirearm = new Weapon();

        playerWeaponInventory = new List<Weapon>();
        completedLevelIDs = new List<string>();
    }
}
