﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Rigidbody2D r2d;
    public float timeOfNextShot;
    public float passedTime;
    public float timeSinceFirstShot;
    Transform shotSpawn;

    int maxHealth;
    public int remainingHealth;
    public int atkDamage;

    public int livesRemaining;

    GameObject[] afterburnerFlames;
    GameObject deathExplosion;

    public bool takingInput;
    public bool gameStarting;

    enum TriggerState
    {
        idle,
        down,
        held,
        up
    }

    TriggerState leftTriggerState;
    TriggerState rightTriggerState;

    Vector2 target;
    public GameObject targetReticle;

    LineRenderer laserBeam;

    public Weapon slot1Weapon;
    public Weapon slot2Weapon;

    public bool usingMouse;

    public float fireRate;

    public float speedMultiplier;

    private void Start()
    {
        //usingMouse = ControlsManager.cm.isUsingMouse; <--- Figure why the hell it's coming up as an error later
        usingMouse = true;
        r2d = gameObject.GetComponent<Rigidbody2D>();
        timeOfNextShot = 0f;
        passedTime = 0f;
        timeSinceFirstShot = 0f;
        shotSpawn = transform.Find("Shot Spawn").GetComponent<Transform>();
        gameObject.transform.SetParent(Camera.main.transform);

        setStats();

        livesRemaining = 3;

        remainingHealth = maxHealth;

        leftTriggerState = TriggerState.idle;
        rightTriggerState = TriggerState.idle;

        target = new Vector2();

        afterburnerFlames = GameObject.FindGameObjectsWithTag("AfterburnerFlame");
        deathExplosion = transform.Find("Explosion Effect").gameObject;

        foreach (GameObject flame in afterburnerFlames)
        {
            flame.SetActive(false);
        }

        deathExplosion.SetActive(false);

        if(usingMouse)
        {
            Cursor.lockState = CursorLockMode.Confined;
            Texture2D reticleTexture = Resources.Load<Texture2D>("textures/cursors/target reticle cursor");
            Cursor.SetCursor(reticleTexture, new Vector2(0.5f * reticleTexture.width, 0.5f * reticleTexture.height), CursorMode.Auto);
            target = Camera.main.ViewportToWorldPoint(Input.mousePosition);
        }

        else
        {
            //Cursor.lockState = CursorLockMode.Locked;
            //Cursor.visible = false;
            targetReticle = Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/target reticle"), Vector3.zero, Quaternion.identity);
            targetReticle.transform.SetParent(Camera.main.transform);
            target = targetReticle.transform.position;
        }

        laserBeam = transform.Find("Shot Spawn/LaserBeam").GetComponent<LineRenderer>();
        laserBeam.useWorldSpace = false;
        laserBeam.enabled = false;

        if (GameControl.control.slot1EquippedFirearm != null)
            slot1Weapon = GameControl.control.slot1EquippedFirearm;

        else
        {
            slot1Weapon = new Weapon();
            slot1Weapon.weaponName = "Standard Aircraft Cannon";
            slot1Weapon.weaponType = 1;
            slot1Weapon.atkIncrease = 3;
            GameControl.control.slot1EquippedFirearm = slot1Weapon;
        }

        if (GameControl.control.slot2EquippedFirearm != null)
            slot2Weapon = GameControl.control.slot2EquippedFirearm;

        Physics2D.IgnoreLayerCollision(0, 10);
        gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    private void FixedUpdate()
    {
        if(takingInput)
        {
            if (usingMouse)
                target = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            else
            {
                if (Input.GetAxis("rightStickH") != 0 || Input.GetAxis("rightStickV") != 0)
                    moveReticleWithRightStick(Input.GetAxis("rightStickH"), Input.GetAxis("rightStickV"));

                target = targetReticle.transform.position;
            }

            Vector2 directionToReticle = target - (Vector2)gameObject.transform.position;
            directionToReticle.Normalize();
            float rotateAngle = Mathf.Atan2(directionToReticle.y, directionToReticle.x);
            rotateAngle = rotateAngle * Mathf.Rad2Deg;
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, rotateAngle);

            // ---------------------------------------------------------------------------------------- \\

            if (Input.GetAxis("Horizontal") < 0)
            {
                r2d.velocity = new Vector2(-6f * speedMultiplier, r2d.velocity.y);
                foreach (GameObject flame in afterburnerFlames)
                {
                    flame.SetActive(true);
                }
            }

            else if (Input.GetAxis("Horizontal") > 0)
            {
                r2d.velocity = new Vector2(6f * speedMultiplier, r2d.velocity.y);
                foreach (GameObject flame in afterburnerFlames)
                {
                    flame.SetActive(true);
                }
            }

            // ---------------------------------------------------------------------------------------- \\

            if (Input.GetAxis("Vertical") > 0)
            {
                r2d.velocity = new Vector2(r2d.velocity.x, 7f * speedMultiplier);
                foreach (GameObject flame in afterburnerFlames)
                {
                    flame.SetActive(true);
                }
            }

            else if (Input.GetAxis("Vertical") < 0)
            {
                r2d.velocity = new Vector2(r2d.velocity.x, -7f * speedMultiplier);
                foreach (GameObject flame in afterburnerFlames)
                {
                    flame.SetActive(true);
                }
            }

            // ---------------------------------------------------------------------------------------- \\

            if (Mathf.Abs(Input.GetAxis("Vertical")) < 0.19f && Mathf.Abs(Input.GetAxis("Horizontal")) < 0.19f)
                foreach (GameObject flame in afterburnerFlames)
                {
                    flame.SetActive(false);
                }

            float finalXPos = 0;
            float finalYPos = 0;

            finalXPos = Camera.main.WorldToScreenPoint(gameObject.transform.position).x;
            finalYPos = Camera.main.WorldToScreenPoint(gameObject.transform.position).y;

            finalXPos = Mathf.Clamp(finalXPos, 30, Screen.width - 30f);
            finalYPos = Mathf.Clamp(finalYPos, 30, Screen.height - 30f);

            gameObject.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(finalXPos, finalYPos));
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, 2.5f);
        }
    }

    private void Update()
    {
        if(takingInput)
            if (Input.GetAxis("Fire1") > 0 ^ Input.GetAxis("Fire2") > 0)
            {
                if (timeSinceFirstShot >= timeOfNextShot)
                {
                    if (Input.GetAxis("Fire1") > 0 && slot1Weapon != null)
                    {
                        fireUsingMode(1, slot1Weapon.weaponType);
                    }

                    else if (Input.GetAxis("Fire2") > 0 && slot2Weapon != null)
                    {
                        fireUsingMode(2, slot2Weapon.weaponType);
                    }
                }
                timeSinceFirstShot += Time.deltaTime;
            }

            else if (Input.GetAxis("Fire1") > 0 && Input.GetAxis("Fire2") > 0)
            {
                if (timeSinceFirstShot >= timeOfNextShot)
                    fireUsingMode(3, combineFireModes(slot1Weapon.weaponType, slot2Weapon.weaponType));

                timeSinceFirstShot += Time.deltaTime;
            }

        // ---------------------------------------------------------------------------------------- \\

        if (usingMouse)
        {
            if (Input.GetButtonUp("Fire1") || Input.GetButtonUp("Fire2"))
            {
                timeSinceFirstShot = 0;
                timeOfNextShot = 0;
                laserBeam.enabled = false;
            }
        }

        else
        {
            switch (leftTriggerState)
            {
                case TriggerState.idle:
                    if (Mathf.Abs(Input.GetAxis("Fire1")) > 0.019f)
                        leftTriggerState = TriggerState.down;
                    break;

                case TriggerState.down:
                    leftTriggerState = TriggerState.held;
                    break;

                case TriggerState.held:
                    if (Mathf.Abs(Input.GetAxis("Fire1"))
                        < 0.019f)
                        leftTriggerState = TriggerState.up;
                    break;

                case TriggerState.up:
                    leftTriggerState = TriggerState.idle;
                    break;
            }

            switch (rightTriggerState)
            {
                case TriggerState.idle:
                    if (Mathf.Abs(Input.GetAxis("Fire2")) > 0.019f)
                        rightTriggerState = TriggerState.down;
                    break;

                case TriggerState.down:
                    rightTriggerState = TriggerState.held;
                    break;

                case TriggerState.held:
                    if (Mathf.Abs(Input.GetAxis("Fire2")) < 0.019f)
                        rightTriggerState = TriggerState.up;
                    break;

                case TriggerState.up:
                    rightTriggerState = TriggerState.idle;
                    break;
            }

            if (leftTriggerState == TriggerState.up || rightTriggerState == TriggerState.up)
            {
                timeSinceFirstShot = 0;
                timeOfNextShot = 0;
                laserBeam.enabled = false;
            }

            // ---------------------------------------------------------------------------------------- \\
        }

        if (remainingHealth <= 0)
            Die();
    }

    void fireUsingMode(int weaponSlot, int mode)
    {
        if (mode == 1)
        {
            fireRate = 3;

            GameObject bulletClone = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/bullet"), shotSpawn.position, shotSpawn.rotation);
            bulletClone.GetComponent<Rigidbody2D>().gravityScale = 0;
            bulletClone.GetComponent<Rigidbody2D>().freezeRotation = true;

            if (weaponSlot == 1)
                bulletClone.GetComponent<Bullet>().damageToDeal = atkDamage + GameControl.control.slot1EquippedFirearm.atkIncrease;

            else if (weaponSlot == 2)
                bulletClone.GetComponent<Bullet>().damageToDeal = atkDamage + GameControl.control.slot2EquippedFirearm.atkIncrease;

            Vector2 direction = target - (Vector2)shotSpawn.position;
            direction.Normalize();

            bulletClone.GetComponent<Bullet>().dir = direction * 20;

            timeOfNextShot += (1f / fireRate);
        }

        else if (mode == 2)
        {
            fireRate = 1.5f;

            Bullet[] bulletArray = new Bullet[3];

            Vector2 direction = target - (Vector2)shotSpawn.position;
            direction.Normalize();

            GameObject bulletClone1 = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/bullet"), shotSpawn.position + 1.5f * ((Vector3)direction), shotSpawn.rotation);
            bulletClone1.GetComponent<Rigidbody2D>().gravityScale = 0;
            bulletClone1.GetComponent<Rigidbody2D>().freezeRotation = true;
            bulletClone1.GetComponent<Bullet>().dir = direction * 20;
            bulletArray[0] = bulletClone1.GetComponent<Bullet>();

            GameObject bulletClone2 = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/bullet"), shotSpawn.position + 0.75f * ((Vector3)direction), shotSpawn.rotation);
            bulletClone2.GetComponent<Rigidbody2D>().gravityScale = 0;
            bulletClone2.GetComponent<Rigidbody2D>().freezeRotation = true;
            bulletClone2.GetComponent<Bullet>().dir = direction * 20;
            bulletArray[1] = bulletClone2.GetComponent<Bullet>();

            GameObject bulletClone3 = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/bullet"), shotSpawn.position, shotSpawn.rotation);
            bulletClone3.GetComponent<Rigidbody2D>().gravityScale = 0;
            bulletClone3.GetComponent<Rigidbody2D>().freezeRotation = true;
            bulletClone3.GetComponent<Bullet>().dir = direction * 20;
            bulletArray[2] = bulletClone3.GetComponent<Bullet>();

            foreach(Bullet bullet in bulletArray)
            {
                if (weaponSlot == 1)
                    bullet.damageToDeal = atkDamage + GameControl.control.slot1EquippedFirearm.atkIncrease;

                else if (weaponSlot == 2)
                    bullet.damageToDeal = atkDamage + GameControl.control.slot2EquippedFirearm.atkIncrease;
            }

            timeOfNextShot += (1f / fireRate);
        }

        else if (mode == 3)
        {
            fireRate = 10;

            fireLaser();

            timeOfNextShot += (1f / fireRate);
        }

        else if (mode == 101)
        {
            fireRate = 5;

            Bullet[] bulletArray = new Bullet[3];

            Vector2 direction = target - (Vector2)shotSpawn.position;
            direction.Normalize();

            Vector2 shotLine = new Vector2(direction.y, -direction.x);
            shotLine.Normalize();

            GameObject bulletClone1 = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/bullet"), shotSpawn.position + (Vector3)(0.15f * shotLine), shotSpawn.rotation);
            bulletClone1.GetComponent<Rigidbody2D>().gravityScale = 0;
            bulletClone1.GetComponent<Rigidbody2D>().freezeRotation = true;
            bulletClone1.GetComponent<Bullet>().dir = direction * 20;
            bulletArray[0] = bulletClone1.GetComponent<Bullet>();

            GameObject bulletClone2 = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/bullet"), shotSpawn.position, shotSpawn.rotation);
            bulletClone2.GetComponent<Rigidbody2D>().gravityScale = 0;
            bulletClone2.GetComponent<Rigidbody2D>().freezeRotation = true;
            bulletClone2.GetComponent<Bullet>().dir = direction * 20;
            bulletArray[1] = bulletClone2.GetComponent<Bullet>();

            GameObject bulletClone3 = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/bullet"), shotSpawn.position - (Vector3)(0.15f * shotLine), shotSpawn.rotation);
            bulletClone3.GetComponent<Rigidbody2D>().gravityScale = 0;
            bulletClone3.GetComponent<Rigidbody2D>().freezeRotation = true;
            bulletClone3.GetComponent<Bullet>().dir = direction * 20;
            bulletArray[2] = bulletClone3.GetComponent<Bullet>();

            foreach(Bullet bullet in bulletArray)
            {
                bullet.damageToDeal = Mathf.RoundToInt(atkDamage + (GameControl.control.slot1EquippedFirearm.atkIncrease + GameControl.control.slot2EquippedFirearm.atkIncrease) / 1.5f);
            }

            timeOfNextShot += (1f / fireRate);
        }

        else if (mode == 102)
        {
            fireRate = 8;

            Vector2 direction = target - (Vector2)shotSpawn.position;
            direction.Normalize();

            fireLaser();

            GameObject bulletClone = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/bullet"), shotSpawn.position, shotSpawn.rotation);
            bulletClone.GetComponent<Rigidbody2D>().gravityScale = 0;
            bulletClone.GetComponent<Rigidbody2D>().freezeRotation = true;
            bulletClone.GetComponent<Bullet>().dir = direction * 20;
            bulletClone.GetComponent<Bullet>().damageToDeal = Mathf.RoundToInt(atkDamage + (GameControl.control.slot1EquippedFirearm.atkIncrease + GameControl.control.slot2EquippedFirearm.atkIncrease) / 1.5f);

            timeOfNextShot += (1f / fireRate);
        }

        else if (mode == 103)
        {
            fireRate = 10;

            Vector2 direction = target - (Vector2)shotSpawn.position;
            direction.Normalize();

            Vector2 shotLine = new Vector2(direction.y, -direction.x);
            shotLine.Normalize();

            fireLaser();

            Bullet[] bulletArray = new Bullet[3];

            GameObject bulletClone1 = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/bullet"), shotSpawn.position + (Vector3)(0.4f * shotLine), shotSpawn.rotation);
            bulletClone1.GetComponent<Rigidbody2D>().gravityScale = 0;
            bulletClone1.GetComponent<Rigidbody2D>().freezeRotation = true;
            Vector2 dir1 = new Vector2(target.x - bulletClone1.transform.position.x, target.y - bulletClone1.transform.position.y);
            dir1.Normalize();
            bulletClone1.GetComponent<Bullet>().dir = dir1 * 20;
            bulletArray[0] = bulletClone1.GetComponent<Bullet>();

            GameObject bulletClone2 = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/bullet"), shotSpawn.position, shotSpawn.rotation);
            bulletClone2.GetComponent<Rigidbody2D>().gravityScale = 0;
            bulletClone2.GetComponent<Rigidbody2D>().freezeRotation = true;
            Vector2 dir2 = new Vector2(target.x - bulletClone2.transform.position.x, target.y - bulletClone2.transform.position.y);
            dir2.Normalize();
            bulletClone2.GetComponent<Bullet>().dir = dir2 * 20;
            bulletArray[1] = bulletClone2.GetComponent<Bullet>();

            GameObject bulletClone3 = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/bullet"), shotSpawn.position - (Vector3)(0.4f * shotLine), shotSpawn.rotation);
            bulletClone3.GetComponent<Rigidbody2D>().gravityScale = 0;
            bulletClone3.GetComponent<Rigidbody2D>().freezeRotation = true;
            Vector2 dir3 = new Vector2(target.x - bulletClone3.transform.position.x, target.y - bulletClone3.transform.position.y);
            dir3.Normalize();
            bulletClone3.GetComponent<Bullet>().dir = dir3 * 20;
            bulletArray[2] = bulletClone3.GetComponent<Bullet>();

            foreach (Bullet bullet in bulletArray)
            {
                bullet.damageToDeal = Mathf.RoundToInt(atkDamage + (GameControl.control.slot1EquippedFirearm.atkIncrease + GameControl.control.slot2EquippedFirearm.atkIncrease) / 1.5f);
            }

            timeOfNextShot += (1f / fireRate);
        }
    }

    void moveReticleWithRightStick(float x, float y)
    {
        Vector2 movementDirection = new Vector2(x, y);
        if (movementDirection.sqrMagnitude > 0.1f)
        {
            float newXPos = 0;
            float newYPos = 0;

            newXPos = Camera.main.WorldToScreenPoint(targetReticle.transform.position).x + movementDirection.x * 10;
            newYPos = Camera.main.WorldToScreenPoint(targetReticle.transform.position).y + movementDirection.y * 10;

            newXPos = Mathf.Clamp(newXPos, 0, Screen.width);
            newYPos = Mathf.Clamp(newYPos, 0, Screen.height);

            targetReticle.transform.position = Camera.main.ScreenToWorldPoint(new Vector2(newXPos, newYPos));
            targetReticle.transform.position = new Vector3(targetReticle.transform.position.x, targetReticle.transform.position.y, 0);
            
        }

        else
        {
            return;
        }
    }

    void fireLaser()
    {
        ContactFilter2D cf = new ContactFilter2D();
        cf.layerMask = 10;

        RaycastHit2D[] ray1hitEnemies = new RaycastHit2D[5];
        RaycastHit2D[] ray2hitEnemies = new RaycastHit2D[5];
        RaycastHit2D[] ray3hitEnemies = new RaycastHit2D[5];
        RaycastHit2D[] ray4hitEnemies = new RaycastHit2D[5];
        RaycastHit2D[] ray5hitEnemies = new RaycastHit2D[5];

        GameObject[] hitEnemies = new GameObject[10];

        Vector2 direction = target - (Vector2)shotSpawn.position;
        direction.Normalize();

        Vector2 shotLine = new Vector2(direction.y, -direction.x);
        shotLine.Normalize();

        Physics2D.Raycast(shotSpawn.position, direction, cf, ray1hitEnemies, Mathf.Infinity);
        Physics2D.Raycast(shotSpawn.position + (Vector3)shotLine * 0.2f, direction, cf, ray2hitEnemies, Mathf.Infinity);
        Physics2D.Raycast(shotSpawn.position + (Vector3)shotLine * 0.1f, direction, cf, ray3hitEnemies, Mathf.Infinity);
        Physics2D.Raycast(shotSpawn.position - (Vector3)shotLine * 0.2f, direction, cf, ray4hitEnemies, Mathf.Infinity);
        Physics2D.Raycast(shotSpawn.position - (Vector3)shotLine * 0.1f, direction, cf, ray5hitEnemies, Mathf.Infinity);

        laserBeam.enabled = true;
        laserBeam.SetPosition(0, new Vector3(0, 0, 0));
        laserBeam.SetPosition(1, laserBeam.transform.InverseTransformPoint(gameObject.transform.position) * -100);

        foreach (RaycastHit2D hitEnemy in ray1hitEnemies)
        {
            for (int c = 0; c < hitEnemies.Length; c++)
            {
                if (hitEnemy.transform != null)
                {
                    if (!hitEnemies.Contains(hitEnemy.transform.gameObject))
                    {
                        if (hitEnemies[c] == null)
                        {
                            hitEnemies[c] = hitEnemy.transform.gameObject;
                            break;
                        }
                    }
                }
            }
        }

        foreach (RaycastHit2D hitEnemy in ray2hitEnemies)
        {
            for (int c = 0; c < hitEnemies.Length; c++)
            {
                if (hitEnemy.transform != null)
                {
                    if (!hitEnemies.Contains(hitEnemy.transform.gameObject))
                    {
                        if (hitEnemies[c] == null)
                        {
                            hitEnemies[c] = hitEnemy.transform.gameObject;
                            break;
                        }
                    }
                }
            }
        }

        foreach (RaycastHit2D hitEnemy in ray3hitEnemies)
        {
            for (int c = 0; c < hitEnemies.Length; c++)
            {
                if (hitEnemy.transform != null)
                {
                    if (!hitEnemies.Contains(hitEnemy.transform.gameObject))
                    {
                        if (hitEnemies[c] == null)
                        {
                            hitEnemies[c] = hitEnemy.transform.gameObject;
                            break;
                        }
                    }
                }
            }
        }

        foreach (RaycastHit2D hitEnemy in ray4hitEnemies)
        {
            for (int c = 0; c < hitEnemies.Length; c++)
            {
                if (hitEnemy.transform != null)
                {
                    if (!hitEnemies.Contains(hitEnemy.transform.gameObject))
                    {
                        if (hitEnemies[c] == null)
                        {
                            hitEnemies[c] = hitEnemy.transform.gameObject;
                            break;
                        }
                    }
                }
            }
        }

        foreach (RaycastHit2D hitEnemy in ray5hitEnemies)
        {
            for (int c = 0; c < hitEnemies.Length; c++)
            {
                if (hitEnemy.transform != null)
                {
                    if (!hitEnemies.Contains(hitEnemy.transform.gameObject))
                    {
                        if (hitEnemies[c] == null)
                        {
                            hitEnemies[c] = hitEnemy.transform.gameObject;
                            break;
                        }
                    }
                }
            }
        }

        foreach (GameObject enemy in hitEnemies)
        {
            if (enemy != null)
            {
                if (enemy.GetComponent<EnemyController>() != null && enemy.GetComponent<Renderer>().isVisible)
                    enemy.GetComponent<EnemyController>().remainingHealth--;

                //else if (enemy.GetComponent<EnemyBossController>() != null)
            }
        }
    }

    void setStats()
    {
        if (gameObject.name.Contains("Aquila"))
        {
            maxHealth = 15;
            speedMultiplier = 0.6f;
            atkDamage = 20;
        }

        else if (gameObject.name.Contains("Haliaeetus"))
        {
            maxHealth = 12;
            speedMultiplier = 1.1f;
            atkDamage = 15;
        }

        else if (gameObject.name.Contains("Falco"))
        {
            maxHealth = 6;
            speedMultiplier = 1.5f;
            atkDamage = 12;
        }
    }

    void Die()
    {
        livesRemaining--;

        takingInput = false;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));

        transform.Find("PrimarySprite").gameObject.SetActive(false);
        transform.Find("SecondarySprite").gameObject.SetActive(false);
        transform.Find("TertiarySprite").gameObject.SetActive(false);

        foreach (GameObject flame in afterburnerFlames)
        {
            flame.SetActive(false);
        }

        deathExplosion.SetActive(true);
        deathExplosion.GetComponent<Animator>().SetBool("playerDied", true);

        if (livesRemaining > 1)
            StartCoroutine(respawn());

        else
        {
            //code to end game here
        }
    }

    int combineFireModes(int fireModeA, int fireModeB)
    {
        if ((fireModeA == 1 && fireModeB == 2) || (fireModeA == 2 && fireModeB == 1))
            return 101;

        else if ((fireModeA == 1 && fireModeB == 3) || (fireModeA == 3 && fireModeB == 1))
            return 102;

        else if ((fireModeA == 2 && fireModeB == 3) || (fireModeA == 3 && fireModeB == 2))
            return 103;

        else
            return 0;
    }

    IEnumerator respawn()
    {
        remainingHealth = maxHealth;
        Physics2D.IgnoreLayerCollision(0, 11, true);
        yield return new WaitForSeconds(0.5f);

        deathExplosion.GetComponent<Animator>().SetBool("playerDied", false);
        deathExplosion.gameObject.SetActive(false);

        transform.Find("PrimarySprite").gameObject.SetActive(true);
        transform.Find("SecondarySprite").gameObject.SetActive(true);
        transform.Find("TertiarySprite").gameObject.SetActive(true);

        takingInput = true;
        Physics2D.IgnoreLayerCollision(0, 11, false);
    }
}
