﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLevelControl : MonoBehaviour
{
    public string levelID;

    public int playerLivesRemaining;

    public GameObject speechUIScreen;
    public GameObject timerDisplay;
    public bool gameStarting;

    public GameObject gameOverScreen;
    public GameObject levelClearedScreen;

    public float spawnCoordinateX;
    public float spawnCoordinateY;
    public float spawnCoordinateZ;

    public float timeInSecondsUntilStart;

    public bool levelStarted;
    public bool levelEnded;

    public enum gameMode
    {
        Rush = 1,
        Annihilate = 2,
        Retrieve = 3,
        Eliminate = 4
    }

    public gameMode levelMode;

    private void Start()
    {
        timeInSecondsUntilStart = 3.00f;
        timerDisplay.SetActive(false);
    }

    private void Update()
    {
        if (gameStarting)
        {
            timeInSecondsUntilStart -= Time.deltaTime;
            int remainingTimeCeiledToSec = Mathf.CeilToInt(timeInSecondsUntilStart);
            timerDisplay.GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("textures/UI Objects/CountdownTimerIndicator" + remainingTimeCeiledToSec.ToString());

            if (timeInSecondsUntilStart <= 0)
                timerDisplay.SetActive(false);
        }
    }

    public void StartLevel()
    {
        timeInSecondsUntilStart = 3;
        speechUIScreen.SetActive(false);
        gameStarting = true;
        spawnPlayer();
    }

    public void spawnPlayer()
    {
        GameObject playerObject = Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/" + GameControl.control.selectedClass.ToString()));
        playerObject.transform.position = new Vector3(spawnCoordinateX, spawnCoordinateY, spawnCoordinateZ);
        playerObject.GetComponent<PlayerController>().gameStarting = true;
        playerObject.GetComponent<PlayerController>().takingInput = true;
    }

    public void EndLevel(int endMode)
    {
        // 0 = level cleared, 1 = lives ran out
        if(endMode == 0)
        {
            GameControl.control.completedLevels.Add(levelID);
            print("Added " + levelID + " to completed levels list");
            levelEnded = true;
            levelClearedScreen.SetActive(true);
        }

        else
        {
            levelEnded = true;
            //gameOverScreen.SetActive(true);
        }
    }
}
