﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level1_1Control : GameLevelControl
{
    public GameObject introUI;

    private void Start()
    {
        levelMode = gameMode.Rush;

        print(GameControl.control.completedLevels.Contains("1-1"));
        if (GameControl.control.completedLevels.Contains("1-1"))
        {
            introUI.SetActive(false);
            StartLevel();
        }

        else
            timerDisplay.SetActive(false);

        EnemySpawner spawner = FindObjectOfType<EnemySpawner>();

        spawner.spawnEnemy("expendable fodder", 20, 22, -1, 1);
        spawner.spawnEnemy("expendable fodder", 24, 26, 3, 5);
        spawner.spawnEnemy("expendable fodder", 24, 26, -4, -2);

        spawner.spawnEnemy("expendable agile fodder", 24, 26, -1, 1);
        spawner.spawnEnemy("expendable agile fodder", 20, 22, 3, 5);
        spawner.spawnEnemy("expendable agile fodder", 20, 22, -4, -2);

    }
}