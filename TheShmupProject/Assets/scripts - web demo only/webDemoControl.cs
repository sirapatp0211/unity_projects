﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class webDemoControl : GameControl
{
    public GameObject startScreenCanvas;

    public GameObject countdownCanvas;

    public GameObject inGameCanvas;

    public GameObject scoreTextObject;

    public GameObject endScreenCanvas;
    public GameObject endScreenScoreDisplay;

    public GameObject fadeOutForeground;

    public int score;
    public bool gameEnded;

    private Weapon defaultWep;

    private void Start()
    {
        selectedClass = playerClass.Haliaeetus;
        defaultWep = new Weapon();
        defaultWep.weaponType = 1;
        defaultWep.atkIncrease = 5;

        endScreenCanvas.SetActive(false);
        countdownCanvas.SetActive(false);
    }

    private void Update()
    {
        if(startScreenCanvas.activeInHierarchy && Input.anyKeyDown)
        {
            startScreenCanvas.SetActive(false);
            countdownCanvas.SetActive(true);
            FindObjectOfType<PlayerControlToggler>().gameStarted = true;
            FindObjectOfType<WebDemoEnemySpawner>().gameStarted = true;
            updateScoreDisplay();
        }

        if (gameEnded)
        {
            endScreenCanvas.SetActive(true);
            inGameCanvas.SetActive(false);
        }

        if (endScreenCanvas.activeInHierarchy && Input.anyKeyDown)
            StartCoroutine(resetLevel());
    }

    IEnumerator resetLevel()
    {
        fadeOutForeground.GetComponent<Animator>().SetBool("levelEnd", true);
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void updateScoreDisplay()
    {
        scoreTextObject.GetComponent<Text>().text = score.ToString();
    }
}
