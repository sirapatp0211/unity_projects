﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("enemy"))
            collision.gameObject.GetComponent<Enemy>().hitPoints--;

        else if (collision.gameObject.tag.Equals("enemy boss"))
            collision.gameObject.GetComponent<EnemyBoss>().hitPoints--;

        else if (collision.gameObject.tag.Equals("boss weak point"))
            collision.gameObject.transform.GetComponentInParent<EnemyBoss>().hitPoints -= 2;

        Destroy(gameObject);
    }

    private void Update()
    {
        if (!gameObject.GetComponent<Renderer>().isVisible)
            Destroy(gameObject);
    }
}
