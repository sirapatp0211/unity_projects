﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameControl : MonoBehaviour
{
    public bool gameStarted;

    public int coinsRequired;
    public int coinsInserted;

    public static GameControl control;
    public float timeRemaining;
    public int score;
    public bool keyDropped;
    public string droppedKeyIdentifier;

    public int enemiesToSpawn;
    public int enemiesDestroyed;

    bool bossSpawned;
    public bool bossDefeatedOrTimedOut;

    GameObject timerBox;
    GameObject scoreDisplayBox;
    public GameObject bossWarningBox;

    Text timer;
    Text scoreText;

    private void Start()
    {
        coinsRequired = 1;
        if (control == null)
        {
            control = this;
            DontDestroyOnLoad(this.gameObject);
        }

        else
            Destroy(this.gameObject);

        timeRemaining = 20.00f;
        score = 0;
        enemiesToSpawn = Random.Range(15, 25);
        keyDropped = false;

        timerBox = transform.Find("Canvas/Timer display area").gameObject;
        scoreDisplayBox = transform.Find("Canvas/Score display area").gameObject;
        bossWarningBox = transform.Find("Canvas/Boss alert box").gameObject;

        timer = transform.Find("Canvas/Timer display area/variableText").gameObject.GetComponent<Text>();
        scoreText = transform.Find("Canvas/Score display area/variableText").gameObject.GetComponent<Text>();

        timerBox.SetActive(false);
        scoreDisplayBox.SetActive(false);
        bossWarningBox.SetActive(false);

        timer.text = "20";
        scoreText.text = "0";
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            coinsInserted++;
            print("coin added");
        }

        if (Input.GetKey(KeyCode.LeftControl) && coinsInserted >= coinsRequired)
        {
            timerBox.SetActive(true);
            scoreDisplayBox.SetActive(true);

            gameStarted = true;
        }

        if (gameStarted && SceneManager.GetActiveScene().name.Equals("Main Game"))
        {
            if (timeRemaining > 0f)
            {
                timeRemaining -= Time.deltaTime;
                UpdateTimerDisplay();

                if (bossDefeatedOrTimedOut)
                {
                    //if (keyDropped)
                    //    StartCoroutine(LoadMiniGame(droppedKeyIdentifier));

                    //else
                    //    StartCoroutine(endGame());

                    StartCoroutine(endGame());
                }
            }

            else
            {
                if (enemiesDestroyed >= Mathf.FloorToInt(0.9f * enemiesToSpawn))
                {
                    if (!bossSpawned)
                        StartCoroutine(SpawnBoss());

                    else
                    {
                        if (control.bossDefeatedOrTimedOut)
                        {
                            //if (keyDropped)
                            //    StartCoroutine(LoadMiniGame(droppedKeyIdentifier));

                            //else
                            //    StartCoroutine(endGame());

                            StartCoroutine(endGame());
                        }
                    }
                }

                else
                {
                    //if (keyDropped)
                    //    StartCoroutine(LoadMiniGame(droppedKeyIdentifier));

                    //else
                    //    StartCoroutine(endGame());

                    StartCoroutine(endGame());
                }
            }
        }
    }

    IEnumerator SpawnBoss()
    {
        FindObjectOfType<BossSpawner>().SpawnBoss();
        bossSpawned = true;
        bossWarningBox.SetActive(true);
        yield return new WaitForSeconds(3);
        bossWarningBox.SetActive(false);
        timeRemaining = 15.00f;
    }

    IEnumerator LoadMiniGame(string gameIdentifier)
    {
        //begin fadeout
        yield return new WaitForSeconds(3);
        bossDefeatedOrTimedOut = false;
        SceneManager.LoadScene(gameIdentifier);
    }

    IEnumerator endGame()
    {
        timerBox.SetActive(false);
        scoreDisplayBox.SetActive(false);

        SceneManager.LoadScene("End Screen");

        if (PlayerPrefs.GetInt("isInRedemptionMode") == 0)
        {
            //print out tickets here
        }

        else
        {
            //update leaderboard here
        }
        yield return new WaitForSeconds(10);

        print("Game is reset");

        resetGame();
    }

    public void UpdateScoreDisplay()
    {
        scoreText.text = score.ToString();
    }

    void UpdateTimerDisplay()
    {
        timer.text = Mathf.CeilToInt(timeRemaining).ToString();
    }

    void resetGame()
    {
        gameStarted = false;

        timeRemaining = 20.00f;
        score = 0;
        keyDropped = false;
        droppedKeyIdentifier = "";

        enemiesToSpawn = Random.Range(15, 25);
        enemiesDestroyed = 0;

        bossSpawned = false;
        bossDefeatedOrTimedOut = false;

        SceneManager.LoadScene("Main Game");

        coinsInserted = coinsInserted - coinsRequired;
    }
}
