﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyBoss : MonoBehaviour
{
    public int hitPoints;

    private void Start()
    {
        if (SceneManager.GetActiveScene().name.Equals("Main Game"))
            hitPoints = 60;

        else
            hitPoints = 80;
    }

    private void Update()
    {
        if (gameObject.transform.position.x <= Camera.main.gameObject.transform.position.x + 5)
        {
            float xPos = Camera.main.gameObject.transform.position.x + 5;
            gameObject.transform.position = new Vector3(xPos, gameObject.transform.position.y, gameObject.transform.position.z);
            gameObject.transform.SetParent(Camera.main.gameObject.transform);
        }

        if (hitPoints <= 0 || (GameControl.control.timeRemaining <= 0 && !GameControl.control.bossWarningBox.activeInHierarchy))
            endFight();
    }

    public void endFight()
    {
        //play escape animation

        GetComponent<EdgeCollider2D>().enabled = false;

        if (SceneManager.GetActiveScene().name.Equals("Main Game"))
        {
            if (hitPoints <= 45 && hitPoints > 30)
                GameControl.control.score += 300;

            else if (hitPoints <= 30 && hitPoints > 15)
                GameControl.control.score += 550;

            else if (hitPoints <= 15 && hitPoints > 0)
                GameControl.control.score += 800;

            else if (hitPoints == 0)
                GameControl.control.score += 1000;
        }

        else
        {
            //Give different multiplier wheel to player according to damage done
        }

        GameControl.control.UpdateScoreDisplay();

        GameControl.control.bossDefeatedOrTimedOut = true;
        Destroy(gameObject);
    }
}
