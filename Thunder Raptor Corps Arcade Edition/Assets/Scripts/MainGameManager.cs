﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameManager : MonoBehaviour
{
    EnemySpawn[] possibleSpawnPoints;

    int[] spawnPointsToUse;

    private void Start()
    {
        possibleSpawnPoints = FindObjectsOfType<EnemySpawn>();
        spawnPointsToUse = new int[GameControl.control.enemiesToSpawn];
        bool indexZeroRolled = false;

        for (int i = 0; i < spawnPointsToUse.Length; i++)
        {
            int spawnPointToAdd = Random.Range(0, 24);

            if (spawnPointToAdd == 0)
            {
                if (!indexZeroRolled)
                    indexZeroRolled = true;

                else
                    spawnPointsToUse[i] = Reroll();
            }

            else
            {
                if (!ArrayContainsMatchingElement(spawnPointsToUse, spawnPointToAdd))
                    spawnPointsToUse[i] = spawnPointToAdd;

                else
                    spawnPointsToUse[i] = Reroll();
            }
        }

        foreach (int spawnPointIndex in spawnPointsToUse)
            possibleSpawnPoints[spawnPointIndex].shouldSpawnEnemy = true;
    }

    void addIntToIntArray(int elementToAdd, int[] targetArray)
    {
        int[] temp = new int[targetArray.Length + 1];

        for (int i = 0; i < targetArray.Length; i++)
        {
            temp[i] = targetArray[i];
        }

        temp[temp.Length - 1] = elementToAdd;
        targetArray = temp;
    }

    int Reroll()
    {
        int rerollNumber = 0;
        bool rerollNumberIsInArray = true;

        while (rerollNumberIsInArray)
        {
            rerollNumber = Random.Range(0, 24);
            rerollNumberIsInArray = ArrayContainsMatchingElement(spawnPointsToUse, rerollNumber);
        }

        return rerollNumber;
    }

    bool ArrayContainsMatchingElement(int[] arrayToCheck, int elementToCheckFor)
    {
        for (int i = 0; i < arrayToCheck.Length; i++)
        {
            if (arrayToCheck[i] == elementToCheckFor)
                return true;
        }

        return false;
    }
}
