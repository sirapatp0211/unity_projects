﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    int spawnedEnemyDeterminer;
    public bool shouldSpawnEnemy;

    private void Start()
    {
        spawnedEnemyDeterminer = Random.Range(1, 100);
    }

    private void Update()
    {
        if(GameControl.control.gameStarted)
        {
            if (shouldSpawnEnemy)
            {
                if (spawnedEnemyDeterminer <= 80)
                    spawnEnemy("common enemy");
                else
                    spawnEnemy("uncommon enemy");
            }

            else
                Destroy(gameObject);
        }
    }

    public void spawnEnemy(string enemyName)
    {
        GameObject spawnedEnemy = GameObject.Instantiate(Resources.Load("prefabs/enemy/" + enemyName)) as GameObject;
        spawnedEnemy.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        Destroy(gameObject);
    }
}
