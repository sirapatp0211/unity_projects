﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float fireRate;
    float timeSinceLastShot;

    public int weaponType;

    private void Start()
    {
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        setWeaponTypeTo(1);
    }

    private void Update()
    {
       if (GameControl.control.gameStarted)
       {
            timeSinceLastShot += Time.deltaTime;
            if (Input.GetKey(KeyCode.LeftControl) && timeSinceLastShot >= 1f/fireRate)
            {
                attack();
            }

            if (Input.GetKey(KeyCode.LeftArrow))
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-6, gameObject.GetComponent<Rigidbody2D>().velocity.y);

            else if (Input.GetKey(KeyCode.RightArrow))
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(6, gameObject.GetComponent<Rigidbody2D>().velocity.y);

            if (Input.GetKey(KeyCode.UpArrow))
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(gameObject.GetComponent<Rigidbody2D>().velocity.x, 4);

            else if (Input.GetKey(KeyCode.DownArrow))
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(gameObject.GetComponent<Rigidbody2D>().velocity.x, -4);

            float xPos = gameObject.transform.position.x;
            float yPos = gameObject.transform.position.y;

            Vector2 referencePoint = Camera.main.WorldToScreenPoint(new Vector3(xPos, yPos, 0));

            float clampedXPos = Mathf.Clamp(referencePoint.x, 30, Screen.width - 30);
            float clampedYPos = Mathf.Clamp(referencePoint.y, 30, Screen.height - 30);

            Vector2 referencePointInWorld = Camera.main.ScreenToWorldPoint(new Vector3(clampedXPos, clampedYPos));

            gameObject.transform.position = new Vector3(referencePointInWorld.x, referencePointInWorld.y, gameObject.transform.position.z);
       }
    }

    public void setWeaponTypeTo(int typeID)
    {
        weaponType = typeID;

        if (typeID == 3)
            fireRate = 12;

        else
            fireRate = 3;
    }

    void attack()
    {
        timeSinceLastShot = 0;

        if (weaponType == 1)
        {
            GameObject bullet = GameObject.Instantiate(Resources.Load("prefabs/bullet")) as GameObject;
            bullet.transform.position = transform.Find("ShotSpawn").position;
            bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(30, 0);
        }

        else if (weaponType == 2)
        {
            GameObject bullet1 = GameObject.Instantiate(Resources.Load("prefabs/bullet")) as GameObject;
            GameObject bullet2 = GameObject.Instantiate(Resources.Load("prefabs/bullet")) as GameObject;
            GameObject bullet3 = GameObject.Instantiate(Resources.Load("prefabs/bullet")) as GameObject;
            bullet1.transform.position = transform.Find("ShotSpawn").position;
            bullet1.GetComponent<Rigidbody2D>().velocity = new Vector2(30, 0);

            float bullet2XPos = transform.Find("ShotSpawn").position.x + 2;
            float bullet3XPos = transform.Find("ShotSpawn").position.x + 4;

            bullet2.transform.position = new Vector3(bullet2XPos, transform.Find("ShotSpawn").position.y, transform.Find("ShotSpawn").position.z);
            bullet2.GetComponent<Rigidbody2D>().velocity = new Vector2(30, 0);
            bullet3.transform.position = new Vector3(bullet3XPos, transform.Find("ShotSpawn").position.y, transform.Find("ShotSpawn").position.z);
            bullet3.GetComponent<Rigidbody2D>().velocity = new Vector2(30, 0);
        }

        else if (weaponType == 3)
        {
            
        }

        else if (weaponType == 4)
        {
            
        }
    }
}
