﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpOrb : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            if (gameObject.name.Contains("triple bullet"))
                collision.GetComponent<PlayerController>().setWeaponTypeTo(2);

            else if (gameObject.name.Contains("laser beam"))
                collision.GetComponent<PlayerController>().setWeaponTypeTo(3);

            else if (gameObject.name.Contains("explosive"))
                collision.GetComponent<PlayerController>().setWeaponTypeTo(4);

            Destroy(gameObject);
        }
    }
}
