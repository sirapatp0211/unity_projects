﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    private void Update()
    {
        if (GameControl.control.gameStarted)
            gameObject.transform.position = new Vector3(gameObject.transform.position.x + 4f * Time.deltaTime, gameObject.transform.position.y, gameObject.transform.position.z);
    }
}
