﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossSpawner : MonoBehaviour
{
    GameObject bossToSpawn;

    public void SpawnBoss()
    {
        if (SceneManager.GetActiveScene().name.Equals("Main Game"))
            bossToSpawn = Instantiate(Resources.Load("prefabs/enemy/main game boss")) as GameObject;

        bossToSpawn.transform.position = gameObject.transform.position;
        Destroy(gameObject);
    }
}
