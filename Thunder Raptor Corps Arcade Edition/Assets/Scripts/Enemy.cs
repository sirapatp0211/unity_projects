﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int hitPoints;
    int dropDeterminer;

    private void Start()
    {
        if (gameObject.name.Equals("common enemy") || gameObject.name.Equals("common enemy(Clone)"))
            hitPoints = 1;

        else
            hitPoints = 3;
    }

    private void Update()
    {
        if (hitPoints <= 0)
            Die();
    }

    void Die()
    {
        if(!GameControl.control.keyDropped)
        {
            dropDeterminer = Random.Range(1, 100);

            if (dropDeterminer == 1)
            {
                GameControl.control.droppedKeyIdentifier = "Bonus";
                GameControl.control.keyDropped = true;
            }

            else if (dropDeterminer >= 2 && dropDeterminer <= 4)
            {
                GameControl.control.droppedKeyIdentifier = "B";
                GameControl.control.keyDropped = true;
            }

            else if (dropDeterminer >= 5 && dropDeterminer <= 9)
            {
                GameControl.control.droppedKeyIdentifier = "A";
                GameControl.control.keyDropped = true;
            }

            else
            {

            }

            print("dropped key to " + GameControl.control.droppedKeyIdentifier);
        }

        if (gameObject.name.Equals("common enemy") || gameObject.name.Equals("common enemy(Clone)"))
            GameControl.control.score += 25;

        else
            GameControl.control.score += 75;

        GameControl.control.UpdateScoreDisplay();
        GameControl.control.enemiesDestroyed++;

        Destroy(gameObject);
    }
}
